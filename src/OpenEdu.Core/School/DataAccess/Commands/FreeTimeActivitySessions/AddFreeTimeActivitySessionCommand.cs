﻿using System;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.FreeTimeActivitySessions
{
    /// <summary>
    /// A command for adding a <see cref="FreeTimeActivitySession"/> with the specified properties.
    /// </summary>
    public class AddFreeTimeActivitySessionCommand : ICommand<FreeTimeActivitySession>
    {
        public FreeTimeActivity FreeTimeActivity { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan StartTime { get; set; }

        public int Duration { get; set; }
    }
}