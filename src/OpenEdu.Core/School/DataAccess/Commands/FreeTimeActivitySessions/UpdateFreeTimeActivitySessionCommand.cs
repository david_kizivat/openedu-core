﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.FreeTimeActivitySessions
{
    /// <summary>
    /// A command for updating a <see cref="Entities.FreeTimeActivitySession"/> using its object with updated values.
    /// </summary>
    public class UpdateFreeTimeActivitySessionCommand : ICommand<FreeTimeActivitySession>
    {
        public FreeTimeActivitySession FreeTimeActivitySession { get; set; }
    }
}