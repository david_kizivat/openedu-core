﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.FreeTimeActivitySessions
{
    /// <summary>
    /// A command used to remove a <see cref="FreeTimeActivitySession" /> by its ID.
    /// </summary>
    public class RemoveFreeTimeActivitySessionCommand : ICommand<FreeTimeActivitySession>
    {
        public int Id { get; set; }
    }
}