﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.FreeTimeActivities
{
    /// <summary>
    /// A command for updating a <see cref="FreeTimeActivity"/> using its object with updated values.
    /// </summary>
    public class UpdateFreeTimeActivityCommand : ICommand<FreeTimeActivity>
    {
        public FreeTimeActivity FreeTimeActivity { get; set; }
    }
}
