﻿using System;
using System.Collections.Generic;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.FreeTimeActivities
{
    /// <summary>
    /// A command for adding a <see cref="FreeTimeActivity"/> with the specified properties.
    /// </summary>
    public class AddFreeTimeActivityCommand : ICommand<FreeTimeActivity>
    {
        public string Name { get; set; }

        public string ReferenceName { get; set; }

        public Person Supervisor { get; set; }

        public IList<DateTime> Schedule { get; set; }
    }
}