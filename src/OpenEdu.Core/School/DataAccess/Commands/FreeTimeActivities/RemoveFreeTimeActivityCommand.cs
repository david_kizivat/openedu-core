﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.FreeTimeActivities
{
    /// <summary>
    /// A command used to remove a <see cref="FreeTimeActivity" /> by its ID.
    /// </summary>
    public class RemoveFreeTimeActivityCommand : ICommand<FreeTimeActivity>
    {
        public int Id { get; set; }
    }
}