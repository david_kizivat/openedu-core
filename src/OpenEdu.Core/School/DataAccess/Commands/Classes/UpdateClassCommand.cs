﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.Classes
{
    /// <summary>
    /// A command for updating a <see cref="Class"/> using its object with updated values.
    /// </summary>
    public class UpdateClassCommand : ICommand<Class>
    {
        public Class Class { get; set; }
    }
}