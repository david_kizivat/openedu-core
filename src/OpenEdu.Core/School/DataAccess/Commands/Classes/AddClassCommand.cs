﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Location.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.Classes
{
    /// <summary>
    /// A command for adding a <see cref="Class"/> with the specified properties.
    /// </summary>
    public class AddClassCommand : ICommand<Class>
    {
        public string Name { get; set; }

        public string ReferenceName { get; set; }

        public short Grade { get; set; }

        public Person SupervisingTeacher { get; set; }

        public Room HomeClassroom { get; set; }

        /* Students - not in use yet
        public IList<Person> Students { get; set; }
        */
    }
}