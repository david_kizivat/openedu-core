﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.Classes
{
    /// <summary>
    /// A command used to remove a <see cref="Class" /> by its ID.
    /// </summary>
    public class RemoveClassCommand : ICommand<Class>
    {
        public int Id { get; set; }
    }
}