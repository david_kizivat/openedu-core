﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.Subjects
{
    /// <summary>
    /// A command for updating a <see cref="Subject"/> using its object with updated values.
    /// </summary>
    public class UpdateSubjectCommand : ICommand<Subject>
    {
        public Subject Subject { get; set; }
    }
}
