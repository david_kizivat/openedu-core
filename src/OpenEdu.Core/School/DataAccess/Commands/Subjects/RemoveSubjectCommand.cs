﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.Subjects
{
    /// <summary>
    /// A command used to remove a <see cref="Subject" /> by its ID.
    /// </summary>
    public class RemoveSubjectCommand : ICommand<Subject>
    {
        public int Id { get; set; }
    }
}