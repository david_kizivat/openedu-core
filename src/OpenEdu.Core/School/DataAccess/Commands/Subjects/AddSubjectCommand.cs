﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.Subjects
{
    /// <summary>
    /// A command for adding a <see cref="Subject"/> with the specified properties.
    /// </summary>
    public class AddSubjectCommand : ICommand<Subject>
    {
        public string Name { get; set; }

        public string ReferenceName { get; set; }
    }
}