﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Location;
using OpenEdu.Core.People.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Commands.SchoolInfo
{
    /// <summary>
    /// A command used to update the basic information about the school based on the specified <see cref="Entities.SchoolInfo"/> object.
    /// </summary>
    public class UpdateSchoolInfoCommand : ICommand<Entities.SchoolInfo>
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public Address OfficialAddress { get; set; }

        public string Website { get; set; }

        public Person Headmaster { get; set; }
    }
}