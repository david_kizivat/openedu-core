﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.Classes
{
    /// <summary>
    /// A query that gets all stored <see cref="Class">Classes</see>.
    /// </summary>
    public class GetAllClassesQuery : IQuery<IEnumerable<Class>>
    {
    }
}
