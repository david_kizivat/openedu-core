﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.Classes
{
    /// <summary>
    /// A query for finding a <see cref="Class"/> by its ID.
    /// </summary>
    public class FindClassQuery : IQuery<Class>
    {
        public int Id { get; set; }
    }
}