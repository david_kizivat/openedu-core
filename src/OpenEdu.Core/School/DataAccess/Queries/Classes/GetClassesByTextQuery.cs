﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.Classes
{
    /// <summary>
    /// A query that gets all <see cref="Class">Classes</see> with the specified text 
    /// in their <see cref="Class.Name"/> or <see cref="Class.ReferenceName"/>
    /// </summary>
    public class GetClassesByTextQuery : IQuery<IEnumerable<Class>>
    {
        public string Text { get; set; }
    }
}