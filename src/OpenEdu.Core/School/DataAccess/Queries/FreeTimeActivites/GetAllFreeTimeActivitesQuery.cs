﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivites
{
    /// <summary>
    /// A query that gets all stored <see cref="FreeTimeActivity">FreeTimeActivities</see>.
    /// </summary>
    public class GetAllFreeTimeActivitesQuery : IQuery<IEnumerable<FreeTimeActivity>>
    {
    }
}
