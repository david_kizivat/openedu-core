﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivites
{
    /// <summary>
    /// A query that gets all <see cref="FreeTimeActivity">FreeTimeActivities</see> with the specified text 
    /// in their <see cref="FreeTimeActivity.Name"/> or <see cref="FreeTimeActivity.ReferenceName"/>
    /// </summary>
    public class GetFreeTimeActivitiesByTextQuery : IQuery<IEnumerable<FreeTimeActivity>>
    {
        public string Text { get; set; }
    }
}