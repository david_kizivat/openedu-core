﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivites
{
    /// <summary>
    /// A query for finding a <see cref="FreeTimeActivity"/> by its ID.
    /// </summary>
    public class FindFreeTimeActivityQuery : IQuery<FreeTimeActivity>
    {
        public int Id { get; set; }
    }
}