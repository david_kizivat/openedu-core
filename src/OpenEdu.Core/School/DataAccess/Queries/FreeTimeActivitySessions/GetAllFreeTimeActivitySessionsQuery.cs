﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivitySessions
{
    /// <summary>
    /// A query that gets all stored <see cref="FreeTimeActivitySession">FreeTimeActivities</see>.
    /// </summary>
    public class GetAllFreeTimeActivitySessionsQuery : IQuery<IEnumerable<FreeTimeActivitySession>>
    {
    }
}
