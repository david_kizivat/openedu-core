﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivitySessions
{
    /// <summary>
    /// A query that gets all <see cref="FreeTimeActivitySession">FreeTimeActivities</see>
    /// for the specified <see cref="FreeTimeActivity"/>.
    /// </summary>
    public class GetFreeTimeActivitySessionsByFreeTimeActivityQuery : IQuery<IEnumerable<FreeTimeActivitySession>>
    {
        public FreeTimeActivity FreeTimeActivity { get; set; }
    }
}