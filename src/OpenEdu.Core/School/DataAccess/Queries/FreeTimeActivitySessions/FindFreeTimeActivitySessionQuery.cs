﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivitySessions
{
    /// <summary>
    /// A query for finding a <see cref="FreeTimeActivitySession"/> by its ID.
    /// </summary>
    public class FindFreeTimeActivitySessionQuery : IQuery<FreeTimeActivitySession>
    {
        public int Id { get; set; }
    }
}