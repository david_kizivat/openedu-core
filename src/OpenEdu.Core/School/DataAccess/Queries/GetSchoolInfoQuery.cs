﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries
{
    /// <summary>
    /// A query for getting the basic <see cref="SchoolInfo"/>.
    /// </summary>
    public class GetSchoolInfoQuery : IQuery<SchoolInfo>
    {
    }
}
