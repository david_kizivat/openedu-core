﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.Subjects
{
    /// <summary>
    /// A query that gets all <see cref="Subject">Subjects</see> with the specified text 
    /// in their <see cref="Subject.Name"/> or <see cref="Subject.ReferenceName"/>
    /// </summary>
    public class GetSubjectsByTextQuery : IQuery<IEnumerable<Subject>>
    {
        public string Text { get; set; }
    }
}