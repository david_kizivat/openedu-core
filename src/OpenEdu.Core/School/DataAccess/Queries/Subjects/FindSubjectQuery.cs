﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.Subjects
{
    /// <summary>
    /// A query for finding a <see cref="Subject"/> by its ID.
    /// </summary>
    public class FindSubjectQuery : IQuery<Subject>
    {
        public int Id { get; set; }
    }
}