﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.School.DataAccess.Queries.Subjects
{
    /// <summary>
    /// A query that gets all stored <see cref="Subject">Subjects</see>.
    /// </summary>
    public class GetAllSubjectsQuery : IQuery<IEnumerable<Subject>>
    {
    }
}
