﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Subjects;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.Subjects
{
    public class GetSubjectsByTextQueryHandler : IQueryHandler<GetSubjectsByTextQuery, IEnumerable<Subject>>
    {
        private readonly IRepository<Subject> _subjectRepository;

        public GetSubjectsByTextQueryHandler(IRepository<Subject> subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public IEnumerable<Subject> Handle(GetSubjectsByTextQuery query)
        {
            return _subjectRepository.Query(
                    c => c.Name.Contains(query.Text) ||
                         c.ReferenceName.Contains(query.Text))
                .Select();
        }

        public async Task<IEnumerable<Subject>> HandleAsync(GetSubjectsByTextQuery query)
        {
            return await _subjectRepository.Query(
                    c => c.Name.Contains(query.Text) ||
                         c.ReferenceName.Contains(query.Text))
                .SelectAsync();
        }
    }
}