﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Subjects;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.Subjects
{
    public class FindSubjectQueryHandler : IQueryHandler<FindSubjectQuery, Subject>
    {
        private readonly IRepository<Subject> _subjectRepository;

        public FindSubjectQueryHandler(IRepository<Subject> subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public Subject Handle(FindSubjectQuery query)
        {
            return _subjectRepository.Query(s => s.Id == query.Id).Select().FirstOrDefault();
        }

        public async Task<Subject> HandleAsync(FindSubjectQuery query)
        {
            var resultEnumerable = await _subjectRepository.Query(s => s.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}