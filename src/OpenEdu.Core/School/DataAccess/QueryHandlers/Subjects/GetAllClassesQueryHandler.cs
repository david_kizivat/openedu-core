﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Subjects;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.Subjects
{
    public class GetAllSubjectsQueryHandler : IQueryHandler<GetAllSubjectsQuery, IEnumerable<Subject>>
    {
        private readonly IRepository<Subject> _subjectRepository;

        public GetAllSubjectsQueryHandler(IRepository<Subject> subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public IEnumerable<Subject> Handle(GetAllSubjectsQuery query)
        {
            return _subjectRepository.Query().Select();
        }

        public async Task<IEnumerable<Subject>> HandleAsync(GetAllSubjectsQuery query)
        {
            return await _subjectRepository.Query().SelectAsync();
        }
    }
}