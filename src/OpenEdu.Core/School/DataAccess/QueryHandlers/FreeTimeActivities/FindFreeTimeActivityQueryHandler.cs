﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivites;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.FreeTimeActivities
{
    public class FindFreeTimeActivityQueryHandler : IQueryHandler<FindFreeTimeActivityQuery, FreeTimeActivity>
    {
        private readonly IRepository<FreeTimeActivity> _freeTimeActivityRepository;

        public FindFreeTimeActivityQueryHandler(IRepository<FreeTimeActivity> freeTimeActivityRepository)
        {
            _freeTimeActivityRepository = freeTimeActivityRepository;
        }

        public FreeTimeActivity Handle(FindFreeTimeActivityQuery query)
        {
            return _freeTimeActivityRepository.Query(fta => fta.Id == query.Id).Select().FirstOrDefault();
        }

        public async Task<FreeTimeActivity> HandleAsync(FindFreeTimeActivityQuery query)
        {
            var resultEnumerable = await _freeTimeActivityRepository.Query(fta => fta.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}