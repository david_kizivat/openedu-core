﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivites;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.FreeTimeActivities
{
    public class GetFreeTimeActivitiesByTextQueryHandler : IQueryHandler<GetFreeTimeActivitiesByTextQuery,
        IEnumerable<FreeTimeActivity>>
    {
        private readonly IRepository<FreeTimeActivity> _freeTimeActivityRepository;

        public GetFreeTimeActivitiesByTextQueryHandler(IRepository<FreeTimeActivity> freeTimeActivityRepository)
        {
            _freeTimeActivityRepository = freeTimeActivityRepository;
        }

        public IEnumerable<FreeTimeActivity> Handle(GetFreeTimeActivitiesByTextQuery query)
        {
            return _freeTimeActivityRepository.Query(
                    fta => fta.Name.Contains(query.Text) ||
                           fta.ReferenceName.Contains(query.Text))
                .Select();
        }

        public async Task<IEnumerable<FreeTimeActivity>> HandleAsync(GetFreeTimeActivitiesByTextQuery query)
        {
            return await _freeTimeActivityRepository.Query(
                    fta => fta.Name.Contains(query.Text) ||
                           fta.ReferenceName.Contains(query.Text))
                .SelectAsync();
        }
    }
}