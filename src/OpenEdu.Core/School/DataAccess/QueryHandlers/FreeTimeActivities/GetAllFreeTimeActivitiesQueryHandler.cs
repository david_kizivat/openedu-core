﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivites;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.FreeTimeActivities
{
    public class GetAllFreeTimeActivitiesQueryHandler :
        IQueryHandler<GetAllFreeTimeActivitesQuery, IEnumerable<FreeTimeActivity>>
    {
        private readonly IRepository<FreeTimeActivity> _freeTimeActivityRepository;

        public GetAllFreeTimeActivitiesQueryHandler(IRepository<FreeTimeActivity> freeTimeActivityRepository)
        {
            _freeTimeActivityRepository = freeTimeActivityRepository;
        }

        public IEnumerable<FreeTimeActivity> Handle(GetAllFreeTimeActivitesQuery query)
        {
            return _freeTimeActivityRepository.Query().Select();
        }

        public async Task<IEnumerable<FreeTimeActivity>> HandleAsync(GetAllFreeTimeActivitesQuery query)
        {
            return await _freeTimeActivityRepository.Query().SelectAsync();
        }
    }
}