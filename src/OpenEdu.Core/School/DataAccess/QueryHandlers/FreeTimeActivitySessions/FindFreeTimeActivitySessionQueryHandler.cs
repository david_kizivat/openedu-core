﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivitySessions;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.FreeTimeActivitySessions
{
    public class FindFreeTimeActivitySessionQueryHandler :
        IQueryHandler<FindFreeTimeActivitySessionQuery, FreeTimeActivitySession>
    {
        private readonly IRepository<FreeTimeActivitySession> _freeTimeActivitySessionRepository;

        public FindFreeTimeActivitySessionQueryHandler(
            IRepository<FreeTimeActivitySession> freeTimeActivitySessionRepository)
        {
            _freeTimeActivitySessionRepository = freeTimeActivitySessionRepository;
        }

        public FreeTimeActivitySession Handle(FindFreeTimeActivitySessionQuery query)
        {
            return _freeTimeActivitySessionRepository.Query(fta => fta.Id == query.Id).Select().FirstOrDefault();
        }

        public async Task<FreeTimeActivitySession> HandleAsync(FindFreeTimeActivitySessionQuery query)
        {
            var resultEnumerable =
                await _freeTimeActivitySessionRepository.Query(fta => fta.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}