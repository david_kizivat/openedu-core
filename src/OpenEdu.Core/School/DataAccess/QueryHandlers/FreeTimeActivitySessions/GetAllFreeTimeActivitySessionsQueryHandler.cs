﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivitySessions;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.FreeTimeActivitySessions
{
    public class GetAllFreeTimeActivitySessionsQueryHandler
        : IQueryHandler<GetAllFreeTimeActivitySessionsQuery,
            IEnumerable<FreeTimeActivitySession>>
    {
        private readonly IRepository<FreeTimeActivitySession> _freeTimeActivitySessionRepository;

        public GetAllFreeTimeActivitySessionsQueryHandler(
            IRepository<FreeTimeActivitySession> freeTimeActivitySessionRepository)
        {
            _freeTimeActivitySessionRepository = freeTimeActivitySessionRepository;
        }

        public IEnumerable<FreeTimeActivitySession> Handle(GetAllFreeTimeActivitySessionsQuery query)
        {
            return _freeTimeActivitySessionRepository.Query().Select();
        }

        public async Task<IEnumerable<FreeTimeActivitySession>> HandleAsync(GetAllFreeTimeActivitySessionsQuery query)
        {
            return await _freeTimeActivitySessionRepository.Query().SelectAsync();
        }
    }
}