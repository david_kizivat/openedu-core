﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.FreeTimeActivitySessions;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.FreeTimeActivitySessions
{
    public class GetFreeTimeActivitySessionsByFreeTimeActivityQueryHandler
        : IQueryHandler<GetFreeTimeActivitySessionsByFreeTimeActivityQuery,
            IEnumerable<FreeTimeActivitySession>>
    {
        private readonly IRepository<FreeTimeActivitySession> _freeTimeActivitySessionRepository;

        public GetFreeTimeActivitySessionsByFreeTimeActivityQueryHandler
            (IRepository<FreeTimeActivitySession> freeTimeActivitySessionRepository)
        {
            _freeTimeActivitySessionRepository = freeTimeActivitySessionRepository;
        }

        public IEnumerable<FreeTimeActivitySession> Handle(GetFreeTimeActivitySessionsByFreeTimeActivityQuery query)
        {
            return _freeTimeActivitySessionRepository
                .Query(ftas => ftas.FreeTimeActivity == query.FreeTimeActivity)
                .Select();
        }

        public async Task<IEnumerable<FreeTimeActivitySession>> HandleAsync
            (GetFreeTimeActivitySessionsByFreeTimeActivityQuery query)
        {
            return await _freeTimeActivitySessionRepository
                .Query(ftas => ftas.FreeTimeActivity == query.FreeTimeActivity)
                .SelectAsync();
        }
    }
}