﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Classes;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.Classes
{
    public class FindClassQueryHandler : IQueryHandler<FindClassQuery, Class>
    {
        private readonly IRepository<Class> _classRepository;

        public FindClassQueryHandler(IRepository<Class> classRepository)
        {
            _classRepository = classRepository;
        }

        public Class Handle(FindClassQuery query)
        {
            return _classRepository.Query(c => c.Id == query.Id).Select().FirstOrDefault();
        }

        public async Task<Class> HandleAsync(FindClassQuery query)
        {
            var resultEnumerable = await _classRepository.Query(c => c.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}