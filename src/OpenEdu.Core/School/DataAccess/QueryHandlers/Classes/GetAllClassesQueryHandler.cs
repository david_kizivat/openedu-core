﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Classes;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.Classes
{
    public class GetAllClassesQueryHandler : IQueryHandler<GetAllClassesQuery, IEnumerable<Class>>
    {
        private readonly IRepository<Class> _classRepository;

        public GetAllClassesQueryHandler(IRepository<Class> classRepository)
        {
            _classRepository = classRepository;
        }

        public IEnumerable<Class> Handle(GetAllClassesQuery query)
        {
            return _classRepository.Query().Select();
        }

        public async Task<IEnumerable<Class>> HandleAsync(GetAllClassesQuery query)
        {
            return await _classRepository.Query().SelectAsync();
        }
    }
}