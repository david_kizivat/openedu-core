﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Classes;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers.Classes
{
    public class GetClassesByTextQueryHandler : IQueryHandler<GetClassesByTextQuery, IEnumerable<Class>>
    {
        private readonly IRepository<Class> _classRepository;

        public GetClassesByTextQueryHandler(IRepository<Class> classRepository)
        {
            _classRepository = classRepository;
        }

        public IEnumerable<Class> Handle(GetClassesByTextQuery query)
        {
            return _classRepository.Query(
                    c => c.Name.Contains(query.Text) ||
                         c.ReferenceName.Contains(query.Text))
                .Select();
        }

        public async Task<IEnumerable<Class>> HandleAsync(GetClassesByTextQuery query)
        {
            return await _classRepository.Query(
                    c => c.Name.Contains(query.Text) ||
                         c.ReferenceName.Contains(query.Text))
                .SelectAsync();
        }
    }
}