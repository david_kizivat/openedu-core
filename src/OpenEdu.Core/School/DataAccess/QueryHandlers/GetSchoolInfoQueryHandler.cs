﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.QueryHandlers
{
    public class GetSchoolInfoQueryHandler : IQueryHandler<GetSchoolInfoQuery, SchoolInfo>
    {
        private readonly IRepository<SchoolInfo> _schoolInfoRepository;

        public GetSchoolInfoQueryHandler(IRepository<SchoolInfo> schoolInfoRepository)
        {
            _schoolInfoRepository = schoolInfoRepository;
        }

        public SchoolInfo Handle(GetSchoolInfoQuery query)
        {
            return _schoolInfoRepository.Query().Select().FirstOrDefault();
        }

        public async Task<SchoolInfo> HandleAsync(GetSchoolInfoQuery query)
        {
            var resultCollection = await _schoolInfoRepository.Query().SelectAsync();

            return resultCollection.FirstOrDefault();
        }
    }
}