﻿using Repository.Pattern.Ef6;

namespace OpenEdu.Core.School.DataAccess.Entities
{
    public class SchoolPhoneContact : Entity
    {
        public int Id { get; set; }

        public string Key { get; set; }

        public string PhoneNumber { get; set; }

        public SchoolInfo School { get; set; }
    }
}