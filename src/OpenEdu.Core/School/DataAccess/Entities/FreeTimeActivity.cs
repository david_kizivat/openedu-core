﻿using System.Collections.Generic;
using OpenEdu.Core.People.DataAccess.Entities;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.School.DataAccess.Entities
{
    /// <summary>
    /// Represents an activity pupils can take part if in their free time, mostly on regular basis.
    /// </summary>
    public class FreeTimeActivity : Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ReferenceName { get; set; }

        public Person Tutor { get; set; }

        /// <summary>
        /// Collection of the sessions planned for this <see cref="FreeTimeActivity"/>
        /// </summary>
        public IList<FreeTimeActivitySession> Sessions { get; set; }
    }
}