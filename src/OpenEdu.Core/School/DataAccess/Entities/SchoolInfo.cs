using System.Collections.Generic;
using OpenEdu.Core.Location;
using OpenEdu.Core.People.DataAccess.Entities;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.School.DataAccess.Entities
{
    public class SchoolInfo : Entity
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public Address OfficialAddress { get; set; }

        public string Website { get; set; }

        public Person Headmaster { get; set; }

        public IList<SchoolPhoneContact> SchoolPhoneContacts { get; set; }
    }
}