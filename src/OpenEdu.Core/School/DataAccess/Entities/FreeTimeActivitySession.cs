﻿using System;
using OpenEdu.Core.Time.DataAccess.Enums;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.School.DataAccess.Entities
{
    public class FreeTimeActivitySession : Entity
    {
        public int Id { get; set; }

        public FreeTimeActivity FreeTimeActivity { get; set; }

        public DateTime Date { get; set; }

        public TimeSpan StartTime { get; set; }

        public int DurationInMinutes { get; set; }

        public virtual Periodicity Periodicity { get; set; }
    }
}