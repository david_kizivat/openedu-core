﻿using Repository.Pattern.Ef6;

namespace OpenEdu.Core.School.DataAccess.Entities
{
    public class Subject : Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ReferenceName { get; set; }
    }
}