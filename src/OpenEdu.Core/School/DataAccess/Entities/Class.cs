﻿using OpenEdu.Core.Location.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Entities;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.School.DataAccess.Entities
{
    /// <summary>
    /// The class representing a Class of students in the same grade who share most of the lessons.
    /// </summary>
    public class Class : Entity
    {
        /// <summary>
        /// The unique identificator of the <see cref="Class"/>. 
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the <see cref="Class"/>.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Reference name of the <see cref="Class"/>.
        /// </summary>
        public string ReferenceName { get; set; }

        /// <summary>
        /// The grade of the <see cref="Class"/>.
        /// </summary>
        public short Grade { get; set; }

        /// <summary>
        /// The homeroom teacher of the <see cref="Class"/>.
        /// </summary>
        public Person HomeroomTeacher { get; set; }

        /// <summary>
        /// Home classroom of the <see cref="Class"/>.
        /// </summary>
        public Room HomeClassroom { get; set; }
    }
}