﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Commands.Classes;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.School.DataAccess.Queries.Classes;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.CommandHandlers.Classes
{
    public class UpdateClassCommandHandler : ICommandHandler<UpdateClassCommand>
    {
        private readonly IRepository<Class> _classRepository;
        private readonly IQueryHandler<FindClassQuery, Class> _findClassQueryHandler;

        public UpdateClassCommandHandler(IRepository<Class> classRepository,
            IQueryHandler<FindClassQuery, Class> findClassQueryHandler)
        {
            _classRepository = classRepository;
            _findClassQueryHandler = findClassQueryHandler;
        }

        public void Handle(UpdateClassCommand command)
        {
            var findClassQuery = new FindClassQuery
            {
                Id = command.Class.Id
            };

            var toUpdate = _findClassQueryHandler.HandleAsync(findClassQuery).Result;

            toUpdate.Name = command.Class.Name;
            toUpdate.ReferenceName = command.Class.ReferenceName;
            toUpdate.Grade = command.Class.Grade;
            toUpdate.HomeroomTeacher = command.Class.HomeroomTeacher;
            toUpdate.HomeClassroom = command.Class.HomeClassroom;

            _classRepository.Update(toUpdate);
        }
    }
}