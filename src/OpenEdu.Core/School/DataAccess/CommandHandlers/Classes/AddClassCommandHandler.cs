﻿using OpenEdu.Core.Agenda.CommandHandlers;
using OpenEdu.Core.Agenda.Commands;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Commands.Classes;
using OpenEdu.Core.School.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.CommandHandlers.Classes
{
    /// <summary>
    /// The command handler for <see cref="AddClassCommand"/>.
    /// </summary>
    public class AddClassCommandHandler : ICommandHandler<AddClassCommand>
    {
        private readonly AddGroupCommandHandler _addGroupCommandHandler;

        private readonly IRepository<Class> _classRepository;

        public AddClassCommandHandler(IRepository<Class> classRepository, AddGroupCommandHandler addGroupCommandHandler)
        {
            _classRepository = classRepository;
            _addGroupCommandHandler = addGroupCommandHandler;
        }

        public void Handle(AddClassCommand command)
        {
            var toInsert = new Class
            {
                Name = command.Name,
                ReferenceName = command.ReferenceName,
                Grade = command.Grade,
                HomeroomTeacher = command.SupervisingTeacher,
                HomeClassroom = command.HomeClassroom
            };

            var addGroupCommand = new AddGroupCommand
            {
                Name = command.Name,
                ReferenceName = command.ReferenceName,
                Class = toInsert
            };

            _addGroupCommandHandler.Handle(addGroupCommand);
            _classRepository.Insert(toInsert);
        }
    }
}