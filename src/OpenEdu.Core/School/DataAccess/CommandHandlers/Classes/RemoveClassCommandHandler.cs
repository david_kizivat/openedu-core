﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Commands.Classes;
using OpenEdu.Core.School.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.School.DataAccess.CommandHandlers.Classes
{
    public class RemoveClassCommandHandler : ICommandHandler<RemoveClassCommand>
    {
        private readonly IRepository<Class> _classRepository;

        public RemoveClassCommandHandler(IRepository<Class> classRepository)
        {
            _classRepository = classRepository;
        }
        public void Handle(RemoveClassCommand command)
        {
            _classRepository.Delete(command.Id);
        }
    }
}
