﻿using System;
using System.Data.Entity;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.Integration.DataAccess.Entities;
using OpenEdu.Core.Location.DataAccess.Entities;
using OpenEdu.Core.Location.DataAccess.Enums;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Enums;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Enums;

namespace OpenEdu.Core.DataAccess
{
    public interface IDataContext : IDisposable
    {
        /* School */
        DbSet<SchoolInfo> SchooInfo { get; set; }
        DbSet<Subject> Subjects { get; set; }
        DbSet<Class> Classes { get; set; }
        DbSet<FreeTimeActivity> FreeTimeActivities { get; set; }

        /* Time */
        DbSet<AcademicYear> AcademicYears { get; set; }
        DbSet<Period> Periods { get; set; }

        /* Location */
        DbSet<Building> Buildings { get; set; }
        DbSet<BuildingLevel> BuildingLevels { get; set; }
        DbSet<Room> Rooms { get; set; }

        /* People */
        DbSet<Person> People { get; set; }
        DbSet<Responsibility> Responsibilities { get; set; }

        /* Timetable */
        DbSet<Group> Groups { get; set; }
        DbSet<Lesson> Lessons { get; set; }
        DbSet<Timetable> Timetable { get; set; }

        /* Integration */
        DbSet<DataImportLog> DataImportLog { get; set; }

        /* Enums */
        DbSet<Periodicity> Periodicity { get; set; }
        DbSet<Gender> Genders { get; set; }
        DbSet<FunctionType> FunctionTypes { get; set; }
        DbSet<RoomType> RoomTypes { get; set; }
    }
}