﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.Integration.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Integration
{
    internal class DataImportLogDbModelConfigurator
    {
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<DataImportLog>();

            entityConfigurator.ToTable("DataImportLog", SchemaNames.Integration);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.ImportSourceName)
                .HasColumnOrder(order)
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.ImportStarted)
                .HasColumnOrder(order)
                .IsRequired();

            order++;
            entityConfigurator.Property(x => x.ImportCompleted)
                .HasColumnOrder(order)
                .IsOptional();

            order++;
            entityConfigurator.Property(x => x.ImportFilePath)
                .HasColumnOrder(order)
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.ImportFileSize)
                .HasColumnOrder(order)
                .IsOptional();

            order++;
            entityConfigurator.Property(x => x.ImportFileContent)
                .HasColumnOrder(order)
                .IsOptional()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.ImportedData)
                .HasColumnOrder(order)
                .IsOptional()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.NumberOfObjectsImported)
                .HasColumnOrder(order)
                .IsOptional();
        }
    }
}