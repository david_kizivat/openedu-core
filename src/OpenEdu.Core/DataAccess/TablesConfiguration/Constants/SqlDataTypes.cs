﻿namespace OpenEdu.Core.DataAccess.TablesConfiguration.Constants
{
    public static class SqlDataTypes
    {
        // Exact Numerics
        public static readonly string Bit = "bit";

        public static readonly string TinyInt = "tinyint";

        public static readonly string SmallInt = "smallint";

        public static readonly string Int = "int";

        public static readonly string Numeric = "numeric";

        public static readonly string BigInt = "bigint";

        public static readonly string SmallMoney = "smallmoney";

        public static readonly string Money = "money";

        public static readonly string Decimal = "decimal";

        // Approximate Numerics
        public static readonly string Float = "float";

        public static readonly string Real = "real";

        // Date and Time
        public static readonly string Time = "time";

        public static readonly string Date = "date";

        public static readonly string DateTimeOffset = "datetimeoffset";

        public static readonly string SmallDateTime = "smalldatetime";

        public static readonly string DateTime = "datetime";

        public static readonly string DateTime2 = "datetime2";

        // Character String
        public static readonly string Char = "char";

        public static readonly string VarChar = "varchar";

        public static readonly string Text = "text";

        // Unicode Character String
        public static readonly string NChar = "nchar";

        public static readonly string NVarChar = "nvarchar";

        public static readonly string NText = "ntext";

        // Binary Strings
        public static readonly string Binary = "binary";

        public static readonly string VarBinary = "varbinary";

        public static readonly string Image = "image";

        // Other Data Types
        public static readonly string Cursor = "cursor";

        public static readonly string TimeStamp = "timestamp";

        public static readonly string HierarchyId = "hierarchyid";

        public static readonly string UniqueIdentifier = "uniqueidentifier";

        public static readonly string SqlVariant = "sql_variant";

        public static readonly string Xml = "xml";

        public static readonly string Table = "table";

        // Spatial Types
        public static readonly string Geography = "geography";

        public static readonly string Geometry = "geometry";
    }
}