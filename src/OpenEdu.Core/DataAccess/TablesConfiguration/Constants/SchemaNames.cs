﻿namespace OpenEdu.Core.DataAccess.TablesConfiguration.Constants
{
    public static class SchemaNames
    {
        public static readonly string Dbo = "dbo";

        public static readonly string Time = "Time";

        public static readonly string School = "School";

        public static readonly string People = "People";

        public static readonly string Location = "Location";

        public static readonly string Agenda = "Agenda";

        public static readonly string Administration = "Administration";

        public static readonly string Integration = "Integration";

        public static readonly string EventLog = "EventLog";
    }
}