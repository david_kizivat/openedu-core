﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.School
{
    /// <summary>
    /// Class used to build the <see cref="FreeTimeActivitySession"/> entity model using Fluent API.
    /// </summary>
    public class FreeTimeActivitySessionDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="FreeTimeActivitySession"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<FreeTimeActivitySession>();

            entityConfigurator.ToTable("FreeTimeActivitySessions", SchemaNames.School);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Date)
                .HasColumnOrder(order)
                .IsRequired()
                .HasColumnType(SqlDataTypes.Date);

            order++;
            entityConfigurator.Property(x => x.StartTime)
                .HasColumnOrder(order)
                .IsRequired()
                .HasColumnType(SqlDataTypes.Time);

            order++;
            entityConfigurator.Property(x => x.DurationInMinutes)
                .HasColumnOrder(order)
                .IsRequired();

            // Relationships
            entityConfigurator
                .HasRequired(x => x.FreeTimeActivity)
                .WithMany(x => x.Sessions);

            entityConfigurator
                .HasOptional(x => x.Periodicity)
                .WithOptionalDependent();
        }
    }
}