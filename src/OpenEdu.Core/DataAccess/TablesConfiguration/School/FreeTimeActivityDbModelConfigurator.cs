﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.School
{
    /// <summary>
    /// Class used to build the <see cref="FreeTimeActivity"/> entity model using Fluent API.
    /// </summary>
    public class FreeTimeActivityDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="FreeTimeActivity"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<FreeTimeActivity>();

            entityConfigurator.ToTable("FreeTimeActivities", SchemaNames.School);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(256);

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(5);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Tutor);
        }
    }
}