﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.School
{
    /// <summary>
    /// Class used to build the <see cref="SchoolInfo"/> entity model using Fluent API.
    /// </summary>
    public class SchoolInfoDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="SchoolInfo"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<SchoolInfo>();

            entityConfigurator.ToTable("SchoolInfo", SchemaNames.School);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.FullName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(256);

            order++;
            entityConfigurator.Property(x => x.OfficialAddress.AddressLine1)
                .HasColumnOrder(order)
                .HasColumnName("AddressLine1")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.OfficialAddress.AddressLine2)
                .HasColumnOrder(order)
                .HasColumnName("AddressLine2")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.OfficialAddress.City)
                .HasColumnOrder(order)
                .HasColumnName("City")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.OfficialAddress.CountryName)
                .HasColumnOrder(order)
                .HasColumnName("Country")
                .IsRequired()
                .IsMaxLength();

            /* TODO: Solve PhoneContacts property Db model
            order++;
            entityConfigurator.Property(x => x.PhoneContacts)
                .HasColumnOrder(order)
                .HasColumnType(SqlDataTypes.Time);*/

            order++;
            entityConfigurator.Property(x => x.Website)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(100);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Headmaster);
        }
    }
}