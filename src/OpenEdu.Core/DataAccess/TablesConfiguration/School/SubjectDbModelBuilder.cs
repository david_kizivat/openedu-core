﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.School
{
    /// <summary>
    /// Class used to build the <see cref="Subject"/> entity model using Fluent API.
    /// </summary>
    public static class SubjectDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Subject"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Subject>();

            entityConfigurator.ToTable("Subjects", SchemaNames.School);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(256);

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(5);
        }
    }
}