﻿using System.Data.Entity;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Agenda
{
    /// <summary>
    /// Class used to build the <see cref="Group"/> entity model using Fluent API.
    /// </summary>
    public class GroupDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Group"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Group>();

            entityConfigurator.ToTable("Groups", SchemaNames.Agenda);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(256);

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(5);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Class);
        }
    }
}