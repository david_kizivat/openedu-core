﻿using System.Data.Entity;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Agenda
{
    /// <summary>
    /// Class used to build the <see cref="Timetable"/> entity model using Fluent API.
    /// </summary>
    public class TimetableDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Timetable"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Timetable>();

            entityConfigurator.ToTable("Timetable", SchemaNames.Agenda);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.ValidFrom)
                .HasColumnOrder(order)
                .HasColumnType(SqlDataTypes.Date)
                .IsRequired();

            order++;
            entityConfigurator.Property(x => x.ValidUntil)
                .HasColumnOrder(order)
                .HasColumnType(SqlDataTypes.Date)
                .IsRequired();

            // Relationships
            entityConfigurator
                .HasRequired(x => x.AcademicYear);
        }
    }
}