﻿using System.Data.Entity;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Agenda
{
    /// <summary>
    /// Class used to build the <see cref="Lesson"/> entity model using Fluent API.
    /// </summary>
    public class LessonDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Lesson"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Lesson>();

            entityConfigurator.ToTable("Lessons", SchemaNames.Agenda);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            // Relationships
            entityConfigurator
                .HasMany(x => x.Groups)
                .WithMany()
                .Map(x =>
                {
                    x.MapLeftKey("LessonId");
                    x.MapRightKey("GroupId");
                    x.ToTable("LessonGroups", SchemaNames.Agenda);
                });

            entityConfigurator
                .HasRequired(x => x.Teacher);

            entityConfigurator
                .HasRequired(x => x.Room);

            entityConfigurator
                .HasRequired(x => x.Period);

            entityConfigurator
                .HasRequired(x => x.Periodicity);

            entityConfigurator
                .HasRequired(x => x.Timetable);
        }
    }
}