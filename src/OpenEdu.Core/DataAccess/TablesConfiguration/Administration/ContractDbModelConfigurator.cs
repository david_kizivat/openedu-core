﻿using System.Data.Entity;
using OpenEdu.Core.Administration.DataAccess.Entities;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Administration
{
    public class ContractDbModelConfigurator
    {
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Contract>();

            entityConfigurator.ToTable("Contracts", SchemaNames.Administration);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.StartDate)
                .HasColumnOrder(order)
                .IsRequired();

            order++;
            entityConfigurator.Property(x => x.EndDate)
                .HasColumnOrder(order)
                .IsRequired();

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Person);
        }
    }
}