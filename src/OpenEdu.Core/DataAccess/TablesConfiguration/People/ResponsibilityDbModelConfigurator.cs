﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.People.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.People
{
    /// <summary>
    /// A statuc class used to build the <see cref="Responsibility"/> entity model using Fluent API.
    /// </summary>
    public static class ResponsibilityDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Responsibility"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Responsibility>();

            entityConfigurator.ToTable("Responsibilities", SchemaNames.People);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Title)
                .HasColumnOrder(order)
                .HasMaxLength(256);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.FunctionType);
        }
    }
}