﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.People.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.People
{
    /// <summary>
    /// A statuc class used to build the <see cref="Person"/> entity model using Fluent API.
    /// </summary>
    public static class PersonDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Person"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Person>();

            entityConfigurator.ToTable("People", SchemaNames.People);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name.Prefix)
                .HasColumnOrder(order)
                .HasColumnName("NamePrefix")
                .HasMaxLength(100);

            order++;
            entityConfigurator.Property(x => x.Name.FirstName)
                .HasColumnOrder(order)
                .HasColumnName("FirstName")
                .IsRequired()
                .HasMaxLength(100);

            order++;
            entityConfigurator.Property(x => x.Name.LastName)
                .HasColumnOrder(order)
                .HasColumnName("LastName")
                .IsRequired()
                .HasMaxLength(100);

            order++;
            entityConfigurator.Property(x => x.Name.Prefix)
                .HasColumnOrder(order)
                .HasColumnName("NameSuffix")
                .HasMaxLength(100);

            order++;
            entityConfigurator.Property(x => x.PhoneNumber)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(15);

            order++;
            entityConfigurator.Property(x => x.Email)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(256);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Gender);

            entityConfigurator
                .HasMany(x => x.Contracts)
                .WithRequired(x => x.Person);

            entityConfigurator
                .HasMany(x => x.Responsibilities)
                .WithMany()
                .Map(x =>
                {
                    x.MapLeftKey("PersonId");
                    x.MapRightKey("ResponsibilityId");
                    x.ToTable("PersonResponsibilities", SchemaNames.People);
                });
        }
    }
}