﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Time
{
    /// <summary>
    /// Class used to build the <see cref="Period"/> entity model using Fluent API.
    /// </summary>
    public class PeriodDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Period"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Period>();

            entityConfigurator.ToTable("Periods", SchemaNames.Time);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(20);

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(5);

            order++;
            entityConfigurator.Property(x => x.StartTime)
                .HasColumnOrder(order)
                .IsRequired()
                .HasColumnType(SqlDataTypes.Time);

            order++;
            entityConfigurator.Property(x => x.DurationInMinutes)
                .HasColumnOrder(order)
                .IsRequired();
        }
    }
}