﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Time
{
    /// <summary>
    /// Class used to build the <see cref="AcademicYear"/> entity model using Fluent API.
    /// </summary>
    public static class AcademicYearDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="AcademicYear"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Cinfigure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<AcademicYear>();

            entityConfigurator.ToTable("AcademicYears", SchemaNames.Time);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(10);

            order++;
            entityConfigurator.Property(x => x.StartDate)
                .HasColumnOrder(order)
                .IsRequired()
                .HasColumnType(SqlDataTypes.Date);

            order++;
            entityConfigurator.Property(x => x.EndDate)
                .HasColumnOrder(order)
                .IsRequired()
                .HasColumnType(SqlDataTypes.Date);

            // Ignore
            entityConfigurator.Ignore(x => x.IsCurrent);
        }
    }
}