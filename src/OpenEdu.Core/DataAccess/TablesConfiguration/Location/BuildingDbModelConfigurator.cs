﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.Location.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Location
{
    /// <summary>
    /// Class used to build the <see cref="Building"/> entity model using Fluent API.
    /// </summary>
    public class BuildingDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Building"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Building>();

            entityConfigurator.ToTable("Buildings", SchemaNames.Location);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Address.AddressLine1)
                .HasColumnOrder(order)
                .HasColumnName("AddressLine1")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.Address.AddressLine2)
                .HasColumnOrder(order)
                .HasColumnName("AddressLine2")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.Address.City)
                .HasColumnOrder(order)
                .HasColumnName("AddressCity")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.Address.CountryName)
                .HasColumnOrder(order)
                .HasColumnName("AddressCountry")
                .IsRequired()
                .IsMaxLength();

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(100);

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(5);

            order++;
            entityConfigurator.Property(x => x.Description)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(256);
        }
    }
}