﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.Location.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Location
{
    /// <summary>
    /// Class used to build the <see cref="BuildingLevel"/> entity model using Fluent API.
    /// </summary>
    public class BuildingLevelDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="BuildingLevel"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<BuildingLevel>();

            entityConfigurator.ToTable("BuildingLevels", SchemaNames.Location);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Number)
                .HasColumnOrder(order)
                .IsRequired();

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(5);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Building)
                .WithMany(x => x.Levels);
        }
    }
}