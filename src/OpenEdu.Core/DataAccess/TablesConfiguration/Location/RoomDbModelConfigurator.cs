﻿using System.Data.Entity;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.Location.DataAccess.Entities;

namespace OpenEdu.Core.DataAccess.TablesConfiguration.Location
{
    /// <summary>
    /// Class used to build the <see cref="Room"/> entity model using Fluent API.
    /// </summary>
    public class RoomDbModelConfigurator
    {
        /// <summary>
        /// Builds <see cref="Room"/>'s model.
        /// </summary>
        /// <param name="modelBuilder">The model builder passed from <see cref="DatabaseContext.OnModelCreating"/> method.</param>
        public static void Configure(DbModelBuilder modelBuilder)
        {
            // Entity configuration
            var entityConfigurator = modelBuilder.Entity<Room>();

            entityConfigurator.ToTable("Rooms", SchemaNames.Location);

            // Primary key configuration
            entityConfigurator.HasKey(x => x.Id);

            // Columns configuration
            var order = 0;

            order++;
            entityConfigurator.Property(x => x.Id)
                .HasColumnOrder(order);

            order++;
            entityConfigurator.Property(x => x.Name)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(256);

            order++;
            entityConfigurator.Property(x => x.ReferenceName)
                .HasColumnOrder(order)
                .IsRequired()
                .HasMaxLength(5);

            order++;
            entityConfigurator.Property(x => x.Description)
                .HasColumnOrder(order)
                .IsOptional()
                .HasMaxLength(256);

            // Relationships
            entityConfigurator
                .HasRequired(x => x.Purpose);

            entityConfigurator
                .HasRequired(x => x.Level);
        }
    }
}