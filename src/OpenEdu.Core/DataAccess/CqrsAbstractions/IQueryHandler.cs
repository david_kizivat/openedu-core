﻿using System.Threading.Tasks;

namespace OpenEdu.Core.DataAccess.CqrsAbstractions
{
    /// <summary>
    /// An interface for the query handlers.
    /// </summary>
    /// <typeparam name="TQuery">Type of the query to handle.</typeparam>
    /// <typeparam name="TResult">The result of the query to be returned.</typeparam>
    public interface IQueryHandler<in TQuery, TResult>
    {
        TResult Handle(TQuery query);

        Task<TResult> HandleAsync(TQuery query);
    }
}