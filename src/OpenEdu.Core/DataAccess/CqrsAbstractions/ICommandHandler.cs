﻿namespace OpenEdu.Core.DataAccess.CqrsAbstractions
{
    /// <summary>
    /// An interface for the command handlers.
    /// </summary>
    /// <typeparam name="TCommand">Type of the command to be handled.</typeparam>
    public interface ICommandHandler<in TCommand>
    {
        /// <summary>
        /// Handles the command passed.
        /// </summary>
        /// <param name="command">The command to be handled.</param>
        void Handle(TCommand command);
    }
}