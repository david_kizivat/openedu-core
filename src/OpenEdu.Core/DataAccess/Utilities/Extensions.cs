﻿using System;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Linq;

namespace OpenEdu.Core.DataAccess.Utilities
{
    public static class Extensions
    {
        public static void SeedEnumValues<T, TEnum>(this IDbSet<T> dbSet, Func<TEnum, T> converter)
            where T : class => Enum.GetValues(typeof(TEnum))
            .Cast<object>()
            .Select(value => converter((TEnum) value))
            .ToList()
            .ForEach(instance => dbSet.AddOrUpdate(instance));
    }
}