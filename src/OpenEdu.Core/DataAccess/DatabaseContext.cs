﻿using System.Data.Entity;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.DataAccess.TablesConfiguration.Agenda;
using OpenEdu.Core.DataAccess.TablesConfiguration.Constants;
using OpenEdu.Core.DataAccess.TablesConfiguration.Integration;
using OpenEdu.Core.DataAccess.TablesConfiguration.Location;
using OpenEdu.Core.DataAccess.TablesConfiguration.People;
using OpenEdu.Core.DataAccess.TablesConfiguration.School;
using OpenEdu.Core.DataAccess.TablesConfiguration.Time;
using OpenEdu.Core.Integration.DataAccess.Entities;
using OpenEdu.Core.Location.DataAccess.Entities;
using OpenEdu.Core.Location.DataAccess.Enums;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Enums;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Enums;

namespace OpenEdu.Core.DataAccess
{
    public class DatabaseContext : DbContext, IDataContext
    {
        public DbSet<AcademicYear> AcademicYears { get; set; }
        public DbSet<BuildingLevel> BuildingLevels { get; set; }
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Class> Classes { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Responsibility> Responsibilities { get; set; }
        public DbSet<FreeTimeActivity> FreeTimeActivities { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Period> Periods { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<SchoolInfo> SchooInfo { get; set; }
        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Timetable> Timetable { get; set; }
        public DbSet<DataImportLog> DataImportLog { get; set; }
        public DbSet<Periodicity> Periodicity { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<FunctionType> FunctionTypes { get; set; }
        public DbSet<RoomType> RoomTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(SchemaNames.Dbo);

            // TODO: Refactor the names of the configuration classes
            AcademicYearDbModelConfigurator.Cinfigure(modelBuilder);
            PeriodDbModelConfigurator.Configure(modelBuilder);

            FreeTimeActivityDbModelConfigurator.Configure(modelBuilder);
            FreeTimeActivitySessionDbModelConfigurator.Configure(modelBuilder);
            SchoolInfoDbModelConfigurator.Configure(modelBuilder);
            SubjectDbModelConfigurator.Configure(modelBuilder);

            PersonDbModelConfigurator.Configure(modelBuilder);
            ResponsibilityDbModelConfigurator.Configure(modelBuilder);

            BuildingDbModelConfigurator.Configure(modelBuilder);
            BuildingLevelDbModelConfigurator.Configure(modelBuilder);
            RoomDbModelConfigurator.Configure(modelBuilder);

            GroupDbModelConfigurator.Configure(modelBuilder);
            LessonDbModelConfigurator.Configure(modelBuilder);
            TimetableDbModelConfigurator.Configure(modelBuilder);

            DataImportLogDbModelConfigurator.Configure(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }
    }
}