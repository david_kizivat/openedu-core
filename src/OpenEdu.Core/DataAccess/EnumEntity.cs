﻿using System;
using System.ComponentModel.DataAnnotations;

namespace OpenEdu.Core.DataAccess
{
    public class EnumEntity
    {
        /// <summary>
        /// Value of the <see cref="EnumEntity"/> which is at the same time the identity of the entity.
        /// </summary>
        [Key]
        [MaxLength(50)]
        public string Value { get; set; }

        protected EnumEntity(Enum @enum)
        {
            Value = @enum.ToString();
        }

        protected EnumEntity()
        {
        }
    }
}