using System;
using OpenEdu.Core.People.DataAccess.Entities;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Administration.DataAccess.Entities
{
    public class Contract : Entity
    {
        public int Id { get; set; }

        public Person Person { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}