﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Integration.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Integration.DataAccess.CommandHandlers
{
    internal class AddDataImportLogCommandHandler : ICommandHandler<DataImportLog>
    {
        private readonly IRepository<DataImportLog> _dataImportLogRepository;

        public AddDataImportLogCommandHandler(IRepository<DataImportLog> dataImportLogRepository)
        {
            _dataImportLogRepository = dataImportLogRepository;
        }

        public void Handle(DataImportLog command)
        {
            var toInsert = new DataImportLog
            {
                ImportSourceName = command.ImportSourceName,
                ImportStarted = command.ImportStarted,
                ImportCompleted = command.ImportCompleted,
                ImportFilePath = command.ImportFilePath,
                ImportFileSize = command.ImportFileSize,
                ImportFileContent = command.ImportFileContent,
                ImportedData = command.ImportedData,
                NumberOfObjectsImported = command.NumberOfObjectsImported
            };

            _dataImportLogRepository.Insert(toInsert);
        }
    }
}