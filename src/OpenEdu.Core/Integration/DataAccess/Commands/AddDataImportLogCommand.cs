﻿using System;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Integration.DataAccess.Entities;

namespace OpenEdu.Core.Integration.DataAccess.Commands
{
    internal class AddDataImportLogCommand : ICommand<DataImportLog>
    {
        public string ImportSourceName { get; set; }

        public DateTime ImportStarted { get; set; }

        public DateTime ImportCompleted { get; set; }

        public string ImportFilePath { get; set; }

        public int ImportFileSize { get; set; }

        public string ImportFileContent { get; set; }

        public string ImportedData { get; set; }

        public int NumberOfObjectsImported { get; set; }
    }
}