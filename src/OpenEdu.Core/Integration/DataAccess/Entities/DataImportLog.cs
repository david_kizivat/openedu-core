﻿using System;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Integration.DataAccess.Entities
{
    public class DataImportLog : Entity
    {
        public int Id { get; set; }

        public string ImportSourceName { get; set; }

        public DateTime ImportStarted { get; set; }

        public DateTime ImportCompleted { get; set; }

        public string ImportFilePath { get; set; }

        public int ImportFileSize { get; set; }

        public string ImportFileContent { get; set; }

        public string ImportedData { get; set; }

        public int NumberOfObjectsImported { get; set; }
    }
}