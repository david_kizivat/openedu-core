﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.Integration.DataAccess.Commands;
using OpenEdu.Core.Location.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Enums;
using OpenEdu.Core.School.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Integration.Import.AscTimetables
{
    public class AscTimetablesDataImporter : DataImporterBase
    {
        // TODO: Refactor this class!

        private readonly Dictionary<string, Class> _classIdToClasses;
        private readonly Dictionary<string, Group> _groupIdToGroups;
        private readonly Dictionary<string, Subject> _subjectIdToSubjects;
        private readonly Dictionary<string, Person> _teacherIdToPeople;

        private static List<DayOfWeek> WorkingDaysOfWeek => new List<DayOfWeek>
        {
            DayOfWeek.Monday,
            DayOfWeek.Tuesday,
            DayOfWeek.Wednesday,
            DayOfWeek.Thursday,
            DayOfWeek.Friday
        };

        public AscTimetablesDataImporter()
        {
            _teacherIdToPeople = new Dictionary<string, Person>();
            _classIdToClasses = new Dictionary<string, Class>();
            _subjectIdToSubjects = new Dictionary<string, Subject>();
            _groupIdToGroups = new Dictionary<string, Group>();
        }

        public void Import(XDocument document)
        {
            var addDataImportLogCommand = new AddDataImportLogCommand
            {
                ImportSourceName = "aSc Timetables",
                ImportStarted = DateTime.Now
            };

            if (document == null)
                throw new ArgumentNullException(nameof(document), $"Argument {nameof(document)} cannot be null.");

            var root = document.Root;

            if (root == null)
                throw new ArgumentException("Sumbmited XML document has no root element.");

            if (root.Name.LocalName != "timetable")
                throw new ArgumentException("Submited XML document is not a timetable.");

            var teacherIdDictionary = new Dictionary<string, Person>();

            var periods = root
                .Elements("period")
                .Select(ParsePeriod)
                .ToList();

            var subjects = root
                .Elements("subject")
                .Select(ParseSubject)
                .ToList();

            var teachers = root
                .Elements("teacher")
                .Select(ParsePerson)
                .ToList();

            var classrooms = root
                .Elements("classroom")
                .Select(ParseRoom)
                .ToList();

            var classes = root
                .Elements("class")
                .Select(ParseClass)
                .ToList();

            var groups = root
                .Elements("group")
                .Select(ParseGroup)
                .ToList();

            var lessons = root
                .Elements("lesson")
                .Select(ParseLesson)
                .ToList();
        }

        private Lesson ParseLesson(XElement lessonElement)
        {
            var lesson = new Lesson
            {
                Groups = GetGroupsByAscIds((string) lessonElement.Attribute("groupids"))

                // TODO: Complete Lessons import
            };

            return lesson;
        }

        private IList<Group> GetGroupsByAscIds(string csvGroupIds)
        {
            // These are comma separated values (csv) - split 'em!
            var groupIdsArray = csvGroupIds.Split(',');

            var result = new List<Group>();

            foreach (var groupId in groupIdsArray)
                try
                {
                    Group group;
                    if (_groupIdToGroups.TryGetValue(groupId, out group))
                        result.Add(group);
                    throw new FormatException($"Class with id {groupId} does not exist");
                }
                catch
                {
                    // Ignore
                }

            return result;
        }

        private Group ParseGroup(XElement groupElement)
        {
            var groupClass = GetClassByAscId((string) groupElement.Attribute("classid"));

            var group = new Group
            {
                // TODO: Our names should match class name if the group represents an entire class
                Name = (string) groupElement.Attribute("name"),
                Class = groupClass,
                IsEntireClass = (string) groupElement.Attribute("isentireclass") == "1"
            };

            return group;
        }

        private Class GetClassByAscId(string ascClassId)
        {
            try
            {
                Class @class;
                if (_classIdToClasses.TryGetValue(ascClassId, out @class))
                    return @class;
                throw new FormatException($"Class with id {ascClassId} does not exist");
            }
            catch
            {
                return null;
            }
        }

        private Class ParseClass(XElement classElement)
        {
            var @class = new Class
            {
                Name = (string) classElement.Attribute("name"),
                ReferenceName = (string) classElement.Attribute("short"),
                Grade = short.Parse((string) classElement.Attribute("grade"))
            };

            try
            {
                Person homeroomTeacher;
                if (_teacherIdToPeople.TryGetValue((string) classElement.Attribute("teacherid"), out homeroomTeacher))
                    @class.HomeroomTeacher = homeroomTeacher;
                else
                    throw new FormatException(
                        $"Teacher with id {(string) classElement.Attribute("teacherid")} does not exist.");
            }
            catch
            {
                @class.HomeroomTeacher = null;
            }

            _classIdToClasses.Add((string) classElement.Attribute("id"), @class);

            return @class;
        }

        private static Room ParseRoom(XElement classroomElement)
        {
            var parsedCapacity = (string) classroomElement.Attribute("capacity") == "*"
                ? (int?) null
                : int.Parse((string) classroomElement.Attribute("capacity"));

            return new Room
            {
                Name = (string) classroomElement.Attribute("name"),
                ReferenceName = (string) classroomElement.Attribute("short"),
                Capacity = parsedCapacity
            };
        }

        private Subject ParseSubject(XElement subjectElement)
        {
            var subject = new Subject
            {
                Name = (string) subjectElement.Attribute("name"),
                ReferenceName = (string) subjectElement.Attribute("short")
            };

            _subjectIdToSubjects.Add((string) subjectElement.Attribute("id"), subject);

            return subject;
        }

        private static Period ParsePeriod(XElement periodElement)
        {
            return new Period
            {
                Name = (string) periodElement.Attribute("name"),
                ReferenceName = (string) periodElement.Attribute("short"),
                StartTime = TimeSpan.Parse((string) periodElement.Attribute("starttime")),
                DurationInMinutes = (int)
                    TimeSpan.Parse((string) periodElement.Attribute("starttime"))
                        .Subtract(TimeSpan.Parse((string) periodElement.Attribute("starttime"))).TotalMinutes,
                DaysOfWeek = WorkingDaysOfWeek
            };
        }

        private Person ParsePerson(XElement personElement)
        {
            var splittedFirstName = ((string) personElement.Attribute("name"))
                .Split(new[] {", "}, StringSplitOptions.RemoveEmptyEntries);

            var person = new Person
            {
                Name =
                {
                    Prefix = splittedFirstName[1],
                    FirstName = splittedFirstName[0],
                    LastName = (string) personElement.Attribute("lastname")
                },
                ReferenceName = (string) personElement.Attribute("short"),
                Gender = GetGenderFromLetter((string) personElement.Attribute("short")),
                PhoneNumber = (string) personElement.Attribute("mobile"),
                Email = (string) personElement.Attribute("email")
            };

            _teacherIdToPeople.Add((string) personElement.Attribute("id"), person);

            return person;
        }

        private static Gender GetGenderFromLetter(string attribute)
        {
            switch (attribute)
            {
                case "":
                    return GenderEnum.NotSpecified;
                case "F":
                    return GenderEnum.Female;
                case "M":
                    return GenderEnum.Male;
                default:
                    return GenderEnum.Other;
            }
        }
    }
}