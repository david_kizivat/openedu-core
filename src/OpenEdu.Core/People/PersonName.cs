using System.ComponentModel.DataAnnotations.Schema;

namespace OpenEdu.Core.People
{
    [ComplexType]
    public class PersonName
    {
        public string Prefix { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Suffix { get; set; }

        public static bool Contains(PersonName personName, string text)
        {
            return personName.Prefix.Contains(text) ||
                   personName.FirstName.Contains(text) ||
                   personName.LastName.Contains(text) ||
                   personName.Suffix.Contains(text);
        }
    }
}