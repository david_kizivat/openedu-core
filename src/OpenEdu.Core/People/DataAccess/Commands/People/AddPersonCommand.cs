﻿using System.Collections.Generic;
using OpenEdu.Core.Administration.DataAccess.Entities;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Enums;

namespace OpenEdu.Core.People.DataAccess.Commands.People
{
    public class AddPersonCommand : ICommand<Person>
    {
        public PersonName Name { get; set; }

        public string ReferenceName { get; set; }

        public Gender Gender { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public IList<Contract> Contracts { get; set; }

        public IList<Responsibility> Responsibilities { get; set; }
    }
}