﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;

namespace OpenEdu.Core.People.DataAccess.Commands.People
{
    /// <summary>
    /// Hard delete command for a <see cref="Person"/>. Use only in data retention cases.
    /// </summary>
    public class RemovePersonCommand : ICommand<Person>
    {
        public int Id { get; set; }
    }
}