﻿using System.ComponentModel.DataAnnotations.Schema;
using OpenEdu.Core.DataAccess;

namespace OpenEdu.Core.People.DataAccess.Enums
{
    /// <summary>
    /// This class is used to represent <see cref="FunctionType"/> as an entity in the Entity Framework.
    /// </summary>
    [Table("FunctionTypes", Schema = "People")]
    public class FunctionType : EnumEntity
    {
        protected FunctionType(FunctionTypeEnum @enum) : base(@enum)
        {
        }

        protected FunctionType()
        {
        }

        public static implicit operator FunctionType(FunctionTypeEnum @enum) => new FunctionType(@enum);
    }
}