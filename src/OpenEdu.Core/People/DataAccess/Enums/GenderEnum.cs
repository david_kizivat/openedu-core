﻿namespace OpenEdu.Core.People.DataAccess.Enums
{
    public enum GenderEnum
    {
        NotSpecified,

        Male,

        Female,

        Other
    }
}