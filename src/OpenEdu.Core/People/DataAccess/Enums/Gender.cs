﻿using System.ComponentModel.DataAnnotations.Schema;
using OpenEdu.Core.DataAccess;

namespace OpenEdu.Core.People.DataAccess.Enums
{
    [Table("Genders", Schema = "People")]
    public class Gender : EnumEntity
    {
        protected Gender(GenderEnum @enum) : base(@enum)
        {
        }

        protected Gender()
        {
        }

        public static implicit operator Gender(GenderEnum @enum) => new Gender(@enum);
    }
}