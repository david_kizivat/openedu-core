namespace OpenEdu.Core.People.DataAccess.Enums
{
    public enum FunctionTypeEnum
    {
        NotSpecified,

        Pedagogic,

        SubjectComission,

        RepositoryAdministrator,

        Executive,

        Coordinator,

        FreeTimeActivityTutoring,

        Administrative,

        ManualWork,

        Specialized,

        Other
    }
}