﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;

namespace OpenEdu.Core.People.DataAccess.Queries.People
{
    public class GetPeopleByTextQuery : IQuery<IEnumerable<Person>>
    {
        public string Text { get; set; }
    }
}
