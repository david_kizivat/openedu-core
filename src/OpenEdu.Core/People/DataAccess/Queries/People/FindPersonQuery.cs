﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;

namespace OpenEdu.Core.People.DataAccess.Queries.People
{
    public class FindPersonQuery : IQuery<Person>
    {
        public int Id { get; set; }
    }
}