﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Queries.People;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.People.DataAccess.QueryHandlers.People
{
    public class GetPeopleByTextQueryHandler : IQueryHandler<GetPeopleByTextQuery, IEnumerable<Person>>
    {
        private readonly IRepository<Person> _personRepository;

        public GetPeopleByTextQueryHandler(IRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        public IEnumerable<Person> Handle(GetPeopleByTextQuery query)
        {
            return _personRepository.Query(
                    e => PersonName.Contains(e.Name, query.Text) ||
                         e.Email.ToString().Contains(query.Text) ||
                         e.PhoneNumber.Contains(query.Text))
                .Select();
        }

        public async Task<IEnumerable<Person>> HandleAsync(GetPeopleByTextQuery query)
        {
            return await _personRepository.Query(
                    e => PersonName.Contains(e.Name, query.Text) ||
                         e.Email.ToString().Contains(query.Text) ||
                         e.PhoneNumber.Contains(query.Text))
                .SelectAsync();
        }
    }
}