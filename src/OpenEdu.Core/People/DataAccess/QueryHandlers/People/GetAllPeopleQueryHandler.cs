﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Queries.People;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.People.DataAccess.QueryHandlers.People
{
    public class GetAllPeopleQueryHandler : IQueryHandler<GetAllPeopleQuery, IEnumerable<Person>>
    {
        private readonly IRepository<Person> _personRepository;

        public GetAllPeopleQueryHandler(IRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        public IEnumerable<Person> Handle(GetAllPeopleQuery query)
        {
            return _personRepository.Query().Select();
        }

        public async Task<IEnumerable<Person>> HandleAsync(GetAllPeopleQuery query)
        {
            return await _personRepository.Query().SelectAsync();
        }
    }
}