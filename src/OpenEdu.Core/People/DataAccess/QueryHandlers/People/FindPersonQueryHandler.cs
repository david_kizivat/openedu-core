﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Queries.People;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.People.DataAccess.QueryHandlers.People
{
    public class FindPersonQueryHandler : IQueryHandler<FindPersonQuery, Person>
    {
        private readonly IRepository<Person> _personRepository;

        public FindPersonQueryHandler(IRepository<Person> personRepository)
        {
            _personRepository = personRepository;
        }

        public Person Handle(FindPersonQuery query)
        {
            return _personRepository.Query(e => e.Id == query.Id).Select().FirstOrDefault();
        }

        public async Task<Person> HandleAsync(FindPersonQuery query)
        {
            var resultEnumerable = await _personRepository.Query(e => e.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}