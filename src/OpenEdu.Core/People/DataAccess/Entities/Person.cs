using System.Collections.Generic;
using OpenEdu.Core.Administration.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Enums;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.People.DataAccess.Entities
{
    public class Person : Entity
    {
        public int Id { get; set; }

        public PersonName Name { get; set; }

        public string ReferenceName { get; set; }

        public Gender Gender { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public IList<Contract> Contracts { get; set; }

        public IList<Responsibility> Responsibilities { get; set; }

        public Person()
        {
            Name = new PersonName();
        }
    }
}