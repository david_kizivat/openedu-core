﻿using OpenEdu.Core.People.DataAccess.Enums;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.People.DataAccess.Entities
{
    public class Responsibility : Entity
    {
        /// <summary>
        /// The unique identificator of the <see cref="Responsibility"/>.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Title of the resposibility.
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// Type of the function.
        /// </summary>
        public FunctionType FunctionType { get; set; }

        //public UserFunctions Type { get; set; }
    }
}