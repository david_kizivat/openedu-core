namespace OpenEdu.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "Time.AcademicYears",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 10),
                        StartDate = c.DateTime(nullable: false, storeType: "date"),
                        EndDate = c.DateTime(nullable: false, storeType: "date"),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Location.BuildingLevels",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Short(nullable: false),
                        ReferenceName = c.String(nullable: false, maxLength: 5),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Building_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Location.Buildings", t => t.Building_Id, cascadeDelete: true)
                .Index(t => t.Building_Id);
            
            CreateTable(
                "Location.Buildings",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddressLine1 = c.String(nullable: false),
                        AddressLine2 = c.String(nullable: false),
                        AddressCity = c.String(nullable: false),
                        AddressCountry = c.String(nullable: false),
                        Name = c.String(maxLength: 100),
                        ReferenceName = c.String(nullable: false, maxLength: 5),
                        Description = c.String(maxLength: 256),
                        Address_PostalCode = c.String(),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Location.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        ReferenceName = c.String(nullable: false, maxLength: 5),
                        Description = c.String(maxLength: 256),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Level_Id = c.Int(nullable: false),
                        Purpose_Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Location.BuildingLevels", t => t.Level_Id, cascadeDelete: true)
                .ForeignKey("Location.RoomTypes", t => t.Purpose_Value, cascadeDelete: true)
                .Index(t => t.Level_Id)
                .Index(t => t.Purpose_Value);
            
            CreateTable(
                "Location.RoomTypes",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Value);
            
            CreateTable(
                "dbo.Classes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(),
                        ReferenceName = c.String(),
                        Grade = c.Short(nullable: false),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        HomeClassroom_Id = c.Int(),
                        HomeroomTeacher_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Location.Rooms", t => t.HomeClassroom_Id)
                .ForeignKey("People.People", t => t.HomeroomTeacher_Id)
                .ForeignKey("Agenda.Groups", t => t.Id)
                .Index(t => t.Id)
                .Index(t => t.HomeClassroom_Id)
                .Index(t => t.HomeroomTeacher_Id);
            
            CreateTable(
                "People.People",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FirstName = c.String(nullable: false, maxLength: 100),
                        LastName = c.String(nullable: false, maxLength: 100),
                        NameSuffix = c.String(maxLength: 100),
                        PhoneNumber = c.String(maxLength: 15),
                        Email = c.String(maxLength: 256),
                        Name_Suffix = c.String(),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Gender_Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("People.Genders", t => t.Gender_Value, cascadeDelete: true)
                .Index(t => t.Gender_Value);
            
            CreateTable(
                "dbo.Contracts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Person_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("People.People", t => t.Person_Id, cascadeDelete: true)
                .Index(t => t.Person_Id);
            
            CreateTable(
                "People.Genders",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Value);
            
            CreateTable(
                "People.Responsibilities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Title = c.String(maxLength: 256),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        FunctionType_Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("People.FunctionTypes", t => t.FunctionType_Value, cascadeDelete: true)
                .Index(t => t.FunctionType_Value);
            
            CreateTable(
                "People.FunctionTypes",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Value);
            
            CreateTable(
                "dbo.DataImportLogs",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ImportSourceName = c.String(),
                        ImportStarted = c.DateTime(nullable: false),
                        ImportCompleted = c.DateTime(nullable: false),
                        ImportFilePath = c.String(),
                        ImportFileSize = c.Int(nullable: false),
                        ImportFileContent = c.String(),
                        ImportedData = c.String(),
                        NumberOfObjectsImported = c.Int(nullable: false),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "School.FreeTimeActivities",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        ReferenceName = c.String(nullable: false, maxLength: 5),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Tutor_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("People.People", t => t.Tutor_Id, cascadeDelete: true)
                .Index(t => t.Tutor_Id);
            
            CreateTable(
                "School.FreeTimeActivitySessions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Date = c.DateTime(nullable: false, storeType: "date"),
                        StartTime = c.Time(nullable: false, precision: 7),
                        DurationInMinutes = c.Int(nullable: false),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        FreeTimeActivity_Id = c.Int(nullable: false),
                        Periodicity_Value = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("School.FreeTimeActivities", t => t.FreeTimeActivity_Id, cascadeDelete: true)
                .ForeignKey("Time.Periodicity", t => t.Periodicity_Value)
                .Index(t => t.FreeTimeActivity_Id)
                .Index(t => t.Periodicity_Value);
            
            CreateTable(
                "Time.Periodicity",
                c => new
                    {
                        Value = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Value);
            
            CreateTable(
                "Agenda.Groups",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 256),
                        ReferenceName = c.String(nullable: false, maxLength: 5),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Agenda.Lessons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Group_Id = c.Int(nullable: false),
                        Period_Id = c.Int(nullable: false),
                        Periodicity_Value = c.String(nullable: false, maxLength: 50),
                        Room_Id = c.Int(nullable: false),
                        Teacher_Id = c.Int(nullable: false),
                        Timetable_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Agenda.Groups", t => t.Group_Id, cascadeDelete: true)
                .ForeignKey("Time.Periods", t => t.Period_Id, cascadeDelete: true)
                .ForeignKey("Time.Periodicity", t => t.Periodicity_Value, cascadeDelete: true)
                .ForeignKey("Location.Rooms", t => t.Room_Id, cascadeDelete: true)
                .ForeignKey("People.People", t => t.Teacher_Id, cascadeDelete: true)
                .ForeignKey("Agenda.Timetable", t => t.Timetable_Id, cascadeDelete: true)
                .Index(t => t.Group_Id)
                .Index(t => t.Period_Id)
                .Index(t => t.Periodicity_Value)
                .Index(t => t.Room_Id)
                .Index(t => t.Teacher_Id)
                .Index(t => t.Timetable_Id);
            
            CreateTable(
                "Time.Periods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 20),
                        ReferenceName = c.String(maxLength: 5),
                        StartTime = c.Time(nullable: false, precision: 7),
                        DurationInMinutes = c.Int(nullable: false),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "Agenda.Timetable",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        ValidFrom = c.DateTime(nullable: false, storeType: "date"),
                        ValidUntil = c.DateTime(nullable: false, storeType: "date"),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        AcademicYear_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("Time.AcademicYears", t => t.AcademicYear_Id, cascadeDelete: true)
                .Index(t => t.AcademicYear_Id);
            
            CreateTable(
                "School.SchoolInfo",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FullName = c.String(nullable: false, maxLength: 256),
                        AddressLine1 = c.String(nullable: false),
                        AddressLine2 = c.String(nullable: false),
                        City = c.String(nullable: false),
                        Country = c.String(nullable: false),
                        OfficialAddress_PostalCode = c.String(),
                        Website = c.String(),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        Headmaster_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("People.People", t => t.Headmaster_Id, cascadeDelete: true)
                .Index(t => t.Headmaster_Id);
            
            CreateTable(
                "dbo.SchoolPhoneContacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Key = c.String(),
                        PhoneNumber = c.String(),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                        School_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("School.SchoolInfo", t => t.School_Id)
                .Index(t => t.School_Id);
            
            CreateTable(
                "School.Subjects",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 256),
                        ReferenceName = c.String(nullable: false, maxLength: 5),
                        AddedDateTime = c.DateTime(),
                        SourceName = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PersonResponsibilities",
                c => new
                    {
                        PersonId = c.Int(nullable: false),
                        ResponsibilityId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.PersonId, t.ResponsibilityId })
                .ForeignKey("People.People", t => t.PersonId, cascadeDelete: true)
                .ForeignKey("People.Responsibilities", t => t.ResponsibilityId, cascadeDelete: true)
                .Index(t => t.PersonId)
                .Index(t => t.ResponsibilityId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SchoolPhoneContacts", "School_Id", "School.SchoolInfo");
            DropForeignKey("School.SchoolInfo", "Headmaster_Id", "People.People");
            DropForeignKey("Agenda.Lessons", "Timetable_Id", "Agenda.Timetable");
            DropForeignKey("Agenda.Timetable", "AcademicYear_Id", "Time.AcademicYears");
            DropForeignKey("Agenda.Lessons", "Teacher_Id", "People.People");
            DropForeignKey("Agenda.Lessons", "Room_Id", "Location.Rooms");
            DropForeignKey("Agenda.Lessons", "Periodicity_Value", "Time.Periodicity");
            DropForeignKey("Agenda.Lessons", "Period_Id", "Time.Periods");
            DropForeignKey("Agenda.Lessons", "Group_Id", "Agenda.Groups");
            DropForeignKey("dbo.Classes", "Id", "Agenda.Groups");
            DropForeignKey("School.FreeTimeActivities", "Tutor_Id", "People.People");
            DropForeignKey("School.FreeTimeActivitySessions", "Periodicity_Value", "Time.Periodicity");
            DropForeignKey("School.FreeTimeActivitySessions", "FreeTimeActivity_Id", "School.FreeTimeActivities");
            DropForeignKey("dbo.Classes", "HomeroomTeacher_Id", "People.People");
            DropForeignKey("dbo.PersonResponsibilities", "ResponsibilityId", "People.Responsibilities");
            DropForeignKey("dbo.PersonResponsibilities", "PersonId", "People.People");
            DropForeignKey("People.Responsibilities", "FunctionType_Value", "People.FunctionTypes");
            DropForeignKey("People.People", "Gender_Value", "People.Genders");
            DropForeignKey("dbo.Contracts", "Person_Id", "People.People");
            DropForeignKey("dbo.Classes", "HomeClassroom_Id", "Location.Rooms");
            DropForeignKey("Location.Rooms", "Purpose_Value", "Location.RoomTypes");
            DropForeignKey("Location.Rooms", "Level_Id", "Location.BuildingLevels");
            DropForeignKey("Location.BuildingLevels", "Building_Id", "Location.Buildings");
            DropIndex("dbo.PersonResponsibilities", new[] { "ResponsibilityId" });
            DropIndex("dbo.PersonResponsibilities", new[] { "PersonId" });
            DropIndex("dbo.SchoolPhoneContacts", new[] { "School_Id" });
            DropIndex("School.SchoolInfo", new[] { "Headmaster_Id" });
            DropIndex("Agenda.Timetable", new[] { "AcademicYear_Id" });
            DropIndex("Agenda.Lessons", new[] { "Timetable_Id" });
            DropIndex("Agenda.Lessons", new[] { "Teacher_Id" });
            DropIndex("Agenda.Lessons", new[] { "Room_Id" });
            DropIndex("Agenda.Lessons", new[] { "Periodicity_Value" });
            DropIndex("Agenda.Lessons", new[] { "Period_Id" });
            DropIndex("Agenda.Lessons", new[] { "Group_Id" });
            DropIndex("School.FreeTimeActivitySessions", new[] { "Periodicity_Value" });
            DropIndex("School.FreeTimeActivitySessions", new[] { "FreeTimeActivity_Id" });
            DropIndex("School.FreeTimeActivities", new[] { "Tutor_Id" });
            DropIndex("People.Responsibilities", new[] { "FunctionType_Value" });
            DropIndex("dbo.Contracts", new[] { "Person_Id" });
            DropIndex("People.People", new[] { "Gender_Value" });
            DropIndex("dbo.Classes", new[] { "HomeroomTeacher_Id" });
            DropIndex("dbo.Classes", new[] { "HomeClassroom_Id" });
            DropIndex("dbo.Classes", new[] { "Id" });
            DropIndex("Location.Rooms", new[] { "Purpose_Value" });
            DropIndex("Location.Rooms", new[] { "Level_Id" });
            DropIndex("Location.BuildingLevels", new[] { "Building_Id" });
            DropTable("dbo.PersonResponsibilities");
            DropTable("School.Subjects");
            DropTable("dbo.SchoolPhoneContacts");
            DropTable("School.SchoolInfo");
            DropTable("Agenda.Timetable");
            DropTable("Time.Periods");
            DropTable("Agenda.Lessons");
            DropTable("Agenda.Groups");
            DropTable("Time.Periodicity");
            DropTable("School.FreeTimeActivitySessions");
            DropTable("School.FreeTimeActivities");
            DropTable("dbo.DataImportLogs");
            DropTable("People.FunctionTypes");
            DropTable("People.Responsibilities");
            DropTable("People.Genders");
            DropTable("dbo.Contracts");
            DropTable("People.People");
            DropTable("dbo.Classes");
            DropTable("Location.RoomTypes");
            DropTable("Location.Rooms");
            DropTable("Location.Buildings");
            DropTable("Location.BuildingLevels");
            DropTable("Time.AcademicYears");
        }
    }
}
