using System;
using System.Data.Entity.Migrations;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.Utilities;
using OpenEdu.Core.Location.DataAccess.Enums;
using OpenEdu.Core.People.DataAccess.Enums;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Enums;

namespace OpenEdu.Core.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DatabaseContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DatabaseContext context)
        {
            context.Periodicity.SeedEnumValues<Periodicity, PeriodicityEnum>(@enum => @enum);
            context.Genders.SeedEnumValues<Gender, GenderEnum>(@enum => @enum);
            context.FunctionTypes.SeedEnumValues<FunctionType, FunctionTypeEnum>(@enum => @enum);
            context.RoomTypes.SeedEnumValues<RoomType, RoomTypeEnum>(@enum => @enum);

            context.AcademicYears.Add(new AcademicYear
            {
                Name = "admin",
                StartDate = DateTime.Now,
                EndDate = DateTime.Now
            });

            base.Seed(context);
            context.SaveChanges();
        }
    }
}