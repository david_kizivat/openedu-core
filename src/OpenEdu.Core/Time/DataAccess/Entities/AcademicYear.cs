﻿using System;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Time.DataAccess.Entities
{
    public class AcademicYear : Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool IsCurrent => (StartDate <= DateTime.Today) && (EndDate >= DateTime.Today);
    }
}