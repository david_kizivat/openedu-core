﻿using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Time.DataAccess.Entities
{
    /// <summary>
    /// <see cref="Period"/> is a class representing a time window within which lessons may be held.
    /// </summary>
    public class Period : Entity
    {
        /// <summary>
        /// The unique indentifier of a <see cref="Period"/>.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Name of the period.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Abbreviation for the <see cref="Period"/>.
        /// </summary>
        public string ReferenceName { get; set; }

        /// <summary>
        /// The time when the <see cref="Period"/> starts.
        /// </summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// Duration of the <see cref="Period"/> in minutes.
        /// </summary>
        public int DurationInMinutes { get; set; }

        /// <summary>
        /// Days of week on which the <see cref="Period"/> is present.
        /// </summary>
        public IList<DayOfWeek> DaysOfWeek { get; set; }
    }
}