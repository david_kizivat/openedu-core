﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Commands.AcademicYears;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.AcademicYears;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.CommandHandlers.AcademicYears
{
    public class UpdateAcademicYearCommandHandler : ICommandHandler<UpdateAcademicYearCommand>
    {
        private readonly IRepository<AcademicYear> _academicYearRepository;
        private readonly IQueryHandler<FindAcademicYearQuery, AcademicYear> _findAcademicYearQueryHandler;

        public UpdateAcademicYearCommandHandler(IRepository<AcademicYear> academicYearRepository,
            IQueryHandler<FindAcademicYearQuery, AcademicYear> findAcademicYearQueryHandler)
        {
            _academicYearRepository = academicYearRepository;
            _findAcademicYearQueryHandler = findAcademicYearQueryHandler;
        }

        public void Handle(UpdateAcademicYearCommand command)
        {
            var findAcademicYearQuery = new FindAcademicYearQuery
            {
                Id = command.Id
            };

            var toUpdate = _findAcademicYearQueryHandler.HandleAsync(findAcademicYearQuery).Result;

            toUpdate.StartDate = command.StartDate;
            toUpdate.EndDate = command.EndDate;
            toUpdate.Name = command.Name;

            _academicYearRepository.Update(toUpdate);
        }
    }
}