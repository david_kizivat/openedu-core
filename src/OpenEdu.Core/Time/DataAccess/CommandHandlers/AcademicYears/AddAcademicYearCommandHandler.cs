﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Commands.AcademicYears;
using OpenEdu.Core.Time.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.CommandHandlers.AcademicYears
{
    public class AddAcademicYearCommandHandler : ICommandHandler<AddAcademicYearCommand>
    {
        private readonly IRepository<AcademicYear> _academicYearRepository;

        public AddAcademicYearCommandHandler(IRepository<AcademicYear> academicYearRepository)
        {
            _academicYearRepository = academicYearRepository;
        }

        public void Handle(AddAcademicYearCommand command)
        {
            var toInsert = new AcademicYear
            {
                Name = command.Name,
                StartDate = command.StartDateTime,
                EndDate = command.EndDateTime
            };

            _academicYearRepository.Insert(toInsert);
        }
    }
}