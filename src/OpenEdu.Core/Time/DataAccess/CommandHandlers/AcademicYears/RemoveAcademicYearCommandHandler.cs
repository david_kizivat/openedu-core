﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Commands.AcademicYears;
using OpenEdu.Core.Time.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.CommandHandlers.AcademicYears
{
    public class RemoveAcademicYearCommandHandler : ICommandHandler<RemoveAcademicYearCommand>
    {
        private readonly IRepository<AcademicYear> _academicYearRepository;

        public RemoveAcademicYearCommandHandler(IRepository<AcademicYear> academicYearRepository)
        {
            _academicYearRepository = academicYearRepository;
        }

        public void Handle(RemoveAcademicYearCommand command)
        {
            _academicYearRepository.Delete(command.Id);
        }
    }
}