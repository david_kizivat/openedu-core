﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Commands.Periods;
using OpenEdu.Core.Time.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.CommandHandlers.Periods
{
    public class RemovePeriodCommandHandler : ICommandHandler<RemovePeriodCommand>
    {
        private readonly IRepository<Period> _periodRepository;

        public RemovePeriodCommandHandler(IRepository<Period> periodRepository)
        {
            _periodRepository = periodRepository;
        }

        public void Handle(RemovePeriodCommand command)
        {
            _periodRepository.Delete(command.Id);
        }
    }
}