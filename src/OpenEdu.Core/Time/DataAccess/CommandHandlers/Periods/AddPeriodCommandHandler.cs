﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Commands.Periods;
using OpenEdu.Core.Time.DataAccess.Entities;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.CommandHandlers.Periods
{
    public class AddPeriodCommandHandler : ICommandHandler<AddPeriodCommand>
    {
        private readonly IRepository<Period> _periodRepository;

        public AddPeriodCommandHandler(IRepository<Period> periodRepository)
        {
            _periodRepository = periodRepository;
        }

        public void Handle(AddPeriodCommand command)
        {
            var toInsert = new Period
            {
                Name = command.Name,
                ReferenceName = command.ReferenceName,
                StartTime = command.StartTime,
                DurationInMinutes = command.DurationInMinutes
            };

            _periodRepository.Insert(toInsert);
        }
    }
}