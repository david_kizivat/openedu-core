﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Commands.Periods;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.Periods;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.CommandHandlers.Periods
{
    public class UpdatePeriodCommandHandler : ICommandHandler<UpdatePeriodCommand>
    {
        private readonly IQueryHandler<FindPeriodQuery, Period> _findPeriodQueryHandler;
        private readonly IRepository<Period> _periodRepository;

        public UpdatePeriodCommandHandler(IRepository<Period> periodRepository,
            IQueryHandler<FindPeriodQuery, Period> findPeriodQueryHandler)
        {
            _periodRepository = periodRepository;
            _findPeriodQueryHandler = findPeriodQueryHandler;
        }

        public void Handle(UpdatePeriodCommand command)
        {
            var findPeriodQuery = new FindPeriodQuery
            {
                Id = command.Id
            };

            var toUpdate = _findPeriodQueryHandler.HandleAsync(findPeriodQuery).Result;

            toUpdate.Name = command.Name;
            toUpdate.ReferenceName = command.ReferenceName;
            toUpdate.StartTime = command.StartTime;
            toUpdate.DurationInMinutes = command.DurationInMinutes;

            _periodRepository.Update(toUpdate);
        }
    }
}