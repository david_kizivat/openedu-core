﻿using System;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Commands.AcademicYears
{
    /// <summary>
    /// Command used to update the specified <see cref="AcademicYear"/>.
    /// </summary>
    public class UpdateAcademicYearCommand : ICommand<AcademicYear>
    {
        /// <summary>
        /// ID of the <see cref="AcademicYear"/> to be updated.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// New name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// New start date.
        /// </summary>
        public DateTime StartDate { get; set; }

        /// <summary>
        /// New end date.
        /// </summary>
        public DateTime EndDate { get; set; }
    }
}