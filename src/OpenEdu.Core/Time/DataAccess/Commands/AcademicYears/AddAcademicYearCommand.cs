﻿using System;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Commands.AcademicYears
{
    public class AddAcademicYearCommand : ICommand<AcademicYear>
    {
        public string Name { get; set; }

        public DateTime StartDateTime { get; set; }

        public DateTime EndDateTime { get; set; }
    }
}