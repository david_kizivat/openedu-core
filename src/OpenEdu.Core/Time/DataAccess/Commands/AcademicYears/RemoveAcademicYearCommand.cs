﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Commands.AcademicYears
{
    public class RemoveAcademicYearCommand : ICommand<AcademicYear>
    {
        public int Id { get; set; }
    }
}