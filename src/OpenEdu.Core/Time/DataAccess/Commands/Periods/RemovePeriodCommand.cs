﻿using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Commands.Periods
{
    public class RemovePeriodCommand : ICommand<Period>
    {
        public int Id { get; set; }
    }
}