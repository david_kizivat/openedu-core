﻿using System;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Commands.Periods
{
    /// <summary>
    /// Command used to update the specified <see cref="Period"/>
    /// </summary>
    public class UpdatePeriodCommand : ICommand<Period>
    {
        /// <summary>
        /// ID of the <see cref="Period"/> to be updated with the new values.
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// New name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// New abriviation for the <see cref="Period"/>
        /// </summary>
        public string ReferenceName { get; set; }

        /// <summary>
        /// New start time.
        /// </summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// New duration of the <see cref="Period"/> in minutes.
        /// </summary>
        public int DurationInMinutes { get; set; }
    }
}