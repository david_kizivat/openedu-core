﻿using System;
using System.Collections.Generic;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Commands.Periods
{
    /// <summary>
    /// Command used to create new <see cref="Period"/>.
    /// </summary>
    public class AddPeriodCommand : ICommand<Period>
    {
        /// <summary>
        /// Name of the new <see cref="Period"/>.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Abbriviation of the new <see cref="Period"/>.
        /// </summary>
        public string ReferenceName { get; set; }

        /// <summary>
        /// Start time of the new <see cref="Period"/>.
        /// </summary>
        public TimeSpan StartTime { get; set; }

        /// <summary>
        /// Duration of the new <see cref="Period"/> in minutes.
        /// </summary>
        public int DurationInMinutes { get; set; }

        /// <summary>
        /// Days of week on which the new <see cref="Period"/> is present.
        /// </summary>
        public IList<DayOfWeek> DaysOfWeek { get; set; }
    }
}