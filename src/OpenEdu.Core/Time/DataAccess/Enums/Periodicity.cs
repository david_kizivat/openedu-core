﻿using System.ComponentModel.DataAnnotations.Schema;
using OpenEdu.Core.DataAccess;

namespace OpenEdu.Core.Time.DataAccess.Enums
{
    /// <summary>
    /// This class is used to represent <see cref="PeriodicityEnum"/> as an entity in the Entity Framework.
    /// </summary>
    [Table("Periodicity", Schema = "Time")]
    public class Periodicity : EnumEntity
    {
        protected Periodicity(PeriodicityEnum @enum) : base(@enum)
        {
        }

        protected Periodicity()
        {
        }

        public static implicit operator Periodicity(PeriodicityEnum @enum) => new Periodicity(@enum);
    }
}