﻿namespace OpenEdu.Core.Time.DataAccess.Enums
{
    public enum PeriodicityEnum
    {
        NotSpecified,

        Daily,

        Weekly,

        OtherWeek,

        Monthly,

        Yearly
    }
}