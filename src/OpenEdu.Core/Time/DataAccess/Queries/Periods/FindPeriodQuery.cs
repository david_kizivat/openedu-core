﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Queries.Periods
{
    public class FindPeriodQuery : IQuery<Period>
    {
        public int Id { get; set; }
    }
}