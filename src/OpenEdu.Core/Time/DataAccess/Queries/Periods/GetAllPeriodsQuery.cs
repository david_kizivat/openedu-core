﻿using System.Collections.Generic;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Queries.Periods
{
    public class GetAllPeriodsQuery : IQuery<IEnumerable<Period>>
    {
    }
}