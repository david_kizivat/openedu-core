﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Queries.AcademicYears
{
    public class FindAcademicYearQuery : IQuery<AcademicYear>
    {
        public int Id { get; set; }
    }
}