﻿using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;

namespace OpenEdu.Core.Time.DataAccess.Queries.AcademicYears
{
    public class FindCurrentAcademicYearQuery : IQuery<AcademicYear>
    {
    }
}