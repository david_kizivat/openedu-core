﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.Periods;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.QueryHandlers.Periods
{
    public class FindPeriodQueryHandler : IQueryHandler<FindPeriodQuery, Period>
    {
        private readonly IRepository<Period> _periodRepository;

        public FindPeriodQueryHandler(IRepository<Period> periodRepository)
        {
            _periodRepository = periodRepository;
        }

        public Period Handle(FindPeriodQuery query)
        {
            return _periodRepository.Query(p => p.Id == query.Id).Select().FirstOrDefault();
        }

        public async Task<Period> HandleAsync(FindPeriodQuery query)
        {
            var resultEnumerable = await _periodRepository.Query(p => p.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}