﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.Periods;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.QueryHandlers.Periods
{
    public class GetAllPeriodsQueryHandler : IQueryHandler<GetAllPeriodsQuery, IEnumerable<Period>>
    {
        private readonly IRepository<Period> _periodRepository;

        public GetAllPeriodsQueryHandler(IRepository<Period> periodRepository)
        {
            _periodRepository = periodRepository;
        }

        public IEnumerable<Period> Handle(GetAllPeriodsQuery query)
        {
            return _periodRepository.Query().Select();
        }

        public async Task<IEnumerable<Period>> HandleAsync(GetAllPeriodsQuery query)
        {
            return await _periodRepository.Query().SelectAsync();
        }
    }
}