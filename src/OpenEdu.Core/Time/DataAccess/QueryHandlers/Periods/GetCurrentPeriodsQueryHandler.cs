﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.Periods;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.QueryHandlers.Periods
{
    public class GetCurrentPeriodsQueryHandler : IQueryHandler<FindCurrentPeriodsQuery, IEnumerable<Period>>
    {
        private readonly IRepository<Period> _periodRepository;

        public GetCurrentPeriodsQueryHandler(IRepository<Period> periodRepository)
        {
            _periodRepository = periodRepository;
        }

        public IEnumerable<Period> Handle(FindCurrentPeriodsQuery query)
        {
            return
                _periodRepository.Query(
                        p =>
                            (DateTime.Today.Add(p.StartTime) >= DateTime.Now) &&
                            (DateTime.Today.Add(p.StartTime).AddMinutes(p.DurationInMinutes) < DateTime.Now))
                    .Select();
        }

        public async Task<IEnumerable<Period>> HandleAsync(FindCurrentPeriodsQuery query)
        {
            return
                await _periodRepository.Query(
                        p =>
                            (DateTime.Today.Add(p.StartTime) >= DateTime.Now) &&
                            (DateTime.Today.Add(p.StartTime).AddMinutes(p.DurationInMinutes) < DateTime.Now))
                    .SelectAsync();
        }
    }
}