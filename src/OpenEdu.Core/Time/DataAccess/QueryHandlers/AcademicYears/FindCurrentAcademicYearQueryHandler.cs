﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.AcademicYears;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.QueryHandlers.AcademicYears
{
    public class FindCurrentAcademicYearQueryHandler : IQueryHandler<FindCurrentAcademicYearQuery, AcademicYear>
    {
        private readonly IRepository<AcademicYear> _academicYearRepository;

        public FindCurrentAcademicYearQueryHandler(IRepository<AcademicYear> academicYearRepository)
        {
            _academicYearRepository = academicYearRepository;
        }

        public AcademicYear Handle(FindCurrentAcademicYearQuery query)
        {
            return _academicYearRepository.Query().Select().First(ay => ay.IsCurrent);
        }

        public async Task<AcademicYear> HandleAsync(FindCurrentAcademicYearQuery query)
        {
            var resultEnumerable = await _academicYearRepository.Query(ay => ay.IsCurrent).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}