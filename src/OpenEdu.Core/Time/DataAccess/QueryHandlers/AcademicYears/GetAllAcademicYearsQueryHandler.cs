﻿using System.Collections.Generic;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.AcademicYears;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.QueryHandlers.AcademicYears
{
    public class GetAllAcademicYearsQueryHandler : IQueryHandler<GetAllAcademicYearsQuery, IEnumerable<AcademicYear>>
    {
        private readonly IRepository<AcademicYear> _academicYearRepository;

        public GetAllAcademicYearsQueryHandler(IRepository<AcademicYear> academicYearRepository)
        {
            _academicYearRepository = academicYearRepository;
        }

        public IEnumerable<AcademicYear> Handle(GetAllAcademicYearsQuery query)
        {
            return _academicYearRepository.Query().Select();
        }

        public async Task<IEnumerable<AcademicYear>> HandleAsync(GetAllAcademicYearsQuery query)
        {
            return await _academicYearRepository.Query().SelectAsync();
        }
    }
}