﻿using System.Linq;
using System.Threading.Tasks;
using OpenEdu.Core.DataAccess;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Queries.AcademicYears;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Time.DataAccess.QueryHandlers.AcademicYears
{
    public class FindAcademicYearQueryHandler : IQueryHandler<FindAcademicYearQuery, AcademicYear>
    {
        private readonly IRepository<AcademicYear> _academicYearRepository;

        public FindAcademicYearQueryHandler(IRepository<AcademicYear> academicYearRepository)
        {
            _academicYearRepository = academicYearRepository;
        }

        public AcademicYear Handle(FindAcademicYearQuery query)
        {
            return _academicYearRepository.Query().Select().FirstOrDefault(ay => ay.Id == query.Id);
        }

        public async Task<AcademicYear> HandleAsync(FindAcademicYearQuery query)
        {
            var resultEnumerable = await _academicYearRepository.Query(ay => ay.Id == query.Id).SelectAsync();

            return resultEnumerable.FirstOrDefault();
        }
    }
}