﻿using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using OpenEdu.Core.School.DataAccess.Entities;

namespace OpenEdu.Core.Agenda.Commands
{
    public class AddGroupCommand : ICommand<Group>
    {
        public string Name { get; set; }

        public string ReferenceName { get; set; }

        public Class Class { get; set; }

        public bool IsEntireClass { get; set; }
    }
}