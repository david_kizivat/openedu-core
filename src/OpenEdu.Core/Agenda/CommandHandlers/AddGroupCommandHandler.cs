﻿using OpenEdu.Core.Agenda.Commands;
using OpenEdu.Core.Agenda.DataAccess.Entities;
using OpenEdu.Core.DataAccess.CqrsAbstractions;
using Repository.Pattern.Repositories;

namespace OpenEdu.Core.Agenda.CommandHandlers
{
    public class AddGroupCommandHandler : ICommandHandler<AddGroupCommand>
    {
        private readonly IRepository<Group> _groupRepository;

        public AddGroupCommandHandler(IRepository<Group> groupRepository)
        {
            _groupRepository = groupRepository;
        }

        public void Handle(AddGroupCommand command)
        {
            var toInstert = new Group
            {
                Name = command.Name,
                ReferenceName = command.ReferenceName,
                Class = command.Class,
                IsEntireClass = command.IsEntireClass
            };

            _groupRepository.Insert(toInstert);
        }
    }
}