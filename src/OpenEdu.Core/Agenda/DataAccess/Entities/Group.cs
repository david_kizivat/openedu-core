﻿using OpenEdu.Core.School.DataAccess.Entities;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Agenda.DataAccess.Entities
{
    public class Group : Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ReferenceName { get; set; }

        /// <summary>
        /// The <see cref="School.DataAccess.Entities.Class"/> students of which make up the <see cref="Group"/>.
        /// </summary>
        public Class Class { get; set; }

        public bool IsEntireClass { get; set; }

        /* Students not in use yet.
        public Person Students { get; set; }

        public int NumberOfStudents => Students.Count
        */
    }
}