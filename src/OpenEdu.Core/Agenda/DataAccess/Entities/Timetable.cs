﻿using System;
using System.Collections.Generic;
using OpenEdu.Core.Time.DataAccess.Entities;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Agenda.DataAccess.Entities
{
    public class Timetable : Entity
    {
        public int Id { get; set; }

        public AcademicYear AcademicYear { get; set; }

        public DateTime ValidFrom { get; set; }

        public DateTime ValidUntil { get; set; }

        public IList<Lesson> Lessons { get; set; }

        public bool IsActive => (ValidFrom <= DateTime.Today) && (ValidUntil >= DateTime.Today);
    }
}