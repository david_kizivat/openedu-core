﻿using System.Collections.Generic;
using OpenEdu.Core.Location.DataAccess.Entities;
using OpenEdu.Core.People.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Entities;
using OpenEdu.Core.Time.DataAccess.Enums;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Agenda.DataAccess.Entities
{
    public class Lesson : Entity
    {
        public int Id { get; set; }

        public IList<Group> Groups { get; set; }

        public Person Teacher { get; set; }

        public Room Room { get; set; }

        public Period Period { get; set; }

        public Periodicity Periodicity { get; set; }

        public Timetable Timetable { get; set; }
    }
}