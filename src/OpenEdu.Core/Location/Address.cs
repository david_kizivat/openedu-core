﻿using System.ComponentModel.DataAnnotations.Schema;

namespace OpenEdu.Core.Location
{
    [ComplexType]
    public class Address
    {
        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string City { get; set; }

        public string PostalCode { get; set; }

        //public RegionInfo RegionInfo { get; set; }

        public string CountryName { get; set; }
    }
}