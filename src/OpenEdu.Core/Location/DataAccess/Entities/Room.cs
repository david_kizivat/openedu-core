using OpenEdu.Core.Location.DataAccess.Enums;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Location.DataAccess.Entities
{
    public class Room : Entity
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string ReferenceName { get; set; }

        public int? Capacity { get; set; }

        public BuildingLevel Level { get; set; }

        public string Description { get; set; }

        public RoomType Purpose { get; set; }
    }
}