using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Location.DataAccess.Entities
{
    public class Building : Entity
    {
        public int Id { get; set; }

        public Address Address { get; set; }

        public string Name { get; set; }

        public string ReferenceName { get; set; }

        public IList<BuildingLevel> Levels { get; set; }

        public string Description { get; set; }
    }
}