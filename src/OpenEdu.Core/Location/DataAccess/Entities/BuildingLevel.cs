using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace OpenEdu.Core.Location.DataAccess.Entities
{
    /// <summary>
    /// The class that represents an entity that represents a level in a <see cref="Entities.Building"/>.
    /// </summary>
    public class BuildingLevel : Entity
    {
        /// <summary>
        /// The unique identifier of the <see cref="BuildingLevel"/>
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Number denoting which floor this is (0 means the floor on the ground level).
        /// </summary>
        public short Number { get; set; }

        /// <summary>
        /// An abbriviation to be used to refer to this <see cref="BuildingLevel"/>
        /// </summary>
        public string ReferenceName { get; set; }

        /// <summary>
        /// The building in which the level is present.
        /// </summary>
        public Building Building { get; set; }

        /// <summary>
        /// List of rooms that have entrence to them from this floor.
        /// </summary>
        public IList<Room> Rooms { get; set; }
    }
}