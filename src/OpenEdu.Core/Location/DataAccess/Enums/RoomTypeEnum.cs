namespace OpenEdu.Core.Location.DataAccess.Enums
{
    public enum RoomTypeEnum
    {
        NotSpecified,

        Office,

        Classroom,

        ComputerRoom,

        SubjectSpecificRoom,

        Gym,

        Other
    }
}