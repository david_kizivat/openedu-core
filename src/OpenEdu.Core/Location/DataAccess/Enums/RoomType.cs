﻿using System.ComponentModel.DataAnnotations.Schema;
using OpenEdu.Core.DataAccess;

namespace OpenEdu.Core.Location.DataAccess.Enums
{
    [Table("RoomTypes", Schema = "Location")]
    public class RoomType : EnumEntity
    {
        protected RoomType(RoomTypeEnum @enum) : base(@enum)
        {
        }

        protected RoomType()
        {
        }

        public static implicit operator RoomType(RoomTypeEnum @enum) => new RoomType(@enum);
    }
}