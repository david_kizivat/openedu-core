﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Repository.Pattern.Infrastructure;

namespace Repository.Pattern.Ef6
{
    public abstract class Entity : IObjectState
    {
        public DateTime? AddedDateTime { get; set; }

        [StringLength(50)]
        public string SourceName { get; set; }

        [NotMapped]
        public ObjectState ObjectState { get; set; }
    }
}